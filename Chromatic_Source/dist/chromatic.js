// jquery.event.move
//
// 1.3.6
//
// Stephen Band
//
// Triggers 'movestart', 'move' and 'moveend' events after
// mousemoves following a mousedown cross a distance threshold,
// similar to the native 'dragstart', 'drag' and 'dragend' events.
// Move events are throttled to animation frames. Move event objects
// have the properties:
//
// pageX:
// pageY:   Page coordinates of pointer.
// startX:
// startY:  Page coordinates of pointer at movestart.
// distX:
// distY:  Distance the pointer has moved since movestart.
// deltaX:
// deltaY:  Distance the finger has moved since last event.
// velocityX:
// velocityY:  Average velocity over last few events.


(function (module) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register as an anonymous module.
		define(['jquery'], module);
	} else {
		// Browser globals
		module(jQuery);
	}
})(function(jQuery, undefined){

	var // Number of pixels a pressed pointer travels before movestart
	    // event is fired.
	    threshold = 6,

	    add = jQuery.event.add,

	    remove = jQuery.event.remove,

	    // Just sugar, so we can have arguments in the same order as
	    // add and remove.
	    trigger = function(node, type, data) {
	    	jQuery.event.trigger(type, data, node);
	    },

	    // Shim for requestAnimationFrame, falling back to timer. See:
	    // see http://paulirish.com/2011/requestanimationframe-for-smart-animating/
	    requestFrame = (function(){
	    	return (
	    		window.requestAnimationFrame ||
	    		window.webkitRequestAnimationFrame ||
	    		window.mozRequestAnimationFrame ||
	    		window.oRequestAnimationFrame ||
	    		window.msRequestAnimationFrame ||
	    		function(fn, element){
	    			return window.setTimeout(function(){
	    				fn();
	    			}, 25);
	    		}
	    	);
	    })(),

	    ignoreTags = {
	    	textarea: true,
	    	input: true,
	    	select: true,
	    	button: true
	    },

	    mouseevents = {
	    	move: 'mousemove',
	    	cancel: 'mouseup dragstart',
	    	end: 'mouseup'
	    },

	    touchevents = {
	    	move: 'touchmove',
	    	cancel: 'touchend',
	    	end: 'touchend'
	    };


	// Constructors

	function Timer(fn){
		var callback = fn,
		    active = false,
		    running = false;

		function trigger(time) {
			if (active){
				callback();
				requestFrame(trigger);
				running = true;
				active = false;
			}
			else {
				running = false;
			}
		}

		this.kick = function(fn) {
			active = true;
			if (!running) { trigger(); }
		};

		this.end = function(fn) {
			var cb = callback;

			if (!fn) { return; }

			// If the timer is not running, simply call the end callback.
			if (!running) {
				fn();
			}
			// If the timer is running, and has been kicked lately, then
			// queue up the current callback and the end callback, otherwise
			// just the end callback.
			else {
				callback = active ?
					function(){ cb(); fn(); } :
					fn ;

				active = true;
			}
		};
	}


	// Functions

	function returnTrue() {
		return true;
	}

	function returnFalse() {
		return false;
	}

	function preventDefault(e) {
		e.preventDefault();
	}

	function preventIgnoreTags(e) {
		// Don't prevent interaction with form elements.
		if (ignoreTags[ e.target.tagName.toLowerCase() ]) { return; }

		e.preventDefault();
	}

	function isLeftButton(e) {
		// Ignore mousedowns on any button other than the left (or primary)
		// mouse button, or when a modifier key is pressed.
		return (e.which === 1 && !e.ctrlKey && !e.altKey);
	}

	function identifiedTouch(touchList, id) {
		var i, l;

		if (touchList.identifiedTouch) {
			return touchList.identifiedTouch(id);
		}

		// touchList.identifiedTouch() does not exist in
		// webkit yet… we must do the search ourselves...

		i = -1;
		l = touchList.length;

		while (++i < l) {
			if (touchList[i].identifier === id) {
				return touchList[i];
			}
		}
	}

	function changedTouch(e, event) {
		var touch = identifiedTouch(e.changedTouches, event.identifier);

		// This isn't the touch you're looking for.
		if (!touch) { return; }

		// Chrome Android (at least) includes touches that have not
		// changed in e.changedTouches. That's a bit annoying. Check
		// that this touch has changed.
		if (touch.pageX === event.pageX && touch.pageY === event.pageY) { return; }

		return touch;
	}


	// Handlers that decide when the first movestart is triggered

	function mousedown(e){
		var data;

		if (!isLeftButton(e)) { return; }

		data = {
			target: e.target,
			startX: e.pageX,
			startY: e.pageY,
			timeStamp: e.timeStamp
		};

		add(document, mouseevents.move, mousemove, data);
		add(document, mouseevents.cancel, mouseend, data);
	}

	function mousemove(e){
		var data = e.data;

		checkThreshold(e, data, e, removeMouse);
	}

	function mouseend(e) {
		removeMouse();
	}

	function removeMouse() {
		remove(document, mouseevents.move, mousemove);
		remove(document, mouseevents.cancel, mouseend);
	}

	function touchstart(e) {
		var touch, template;

		// Don't get in the way of interaction with form elements.
		if (ignoreTags[ e.target.tagName.toLowerCase() ]) { return; }

		touch = e.changedTouches[0];

		// iOS live updates the touch objects whereas Android gives us copies.
		// That means we can't trust the touchstart object to stay the same,
		// so we must copy the data. This object acts as a template for
		// movestart, move and moveend event objects.
		template = {
			target: touch.target,
			startX: touch.pageX,
			startY: touch.pageY,
			timeStamp: e.timeStamp,
			identifier: touch.identifier
		};

		// Use the touch identifier as a namespace, so that we can later
		// remove handlers pertaining only to this touch.
		add(document, touchevents.move + '.' + touch.identifier, touchmove, template);
		add(document, touchevents.cancel + '.' + touch.identifier, touchend, template);
	}

	function touchmove(e){
		var data = e.data,
		    touch = changedTouch(e, data);

		if (!touch) { return; }

		checkThreshold(e, data, touch, removeTouch);
	}

	function touchend(e) {
		var template = e.data,
		    touch = identifiedTouch(e.changedTouches, template.identifier);

		if (!touch) { return; }

		removeTouch(template.identifier);
	}

	function removeTouch(identifier) {
		remove(document, '.' + identifier, touchmove);
		remove(document, '.' + identifier, touchend);
	}


	// Logic for deciding when to trigger a movestart.

	function checkThreshold(e, template, touch, fn) {
		var distX = touch.pageX - template.startX,
		    distY = touch.pageY - template.startY;

		// Do nothing if the threshold has not been crossed.
		if ((distX * distX) + (distY * distY) < (threshold * threshold)) { return; }

		triggerStart(e, template, touch, distX, distY, fn);
	}

	function handled() {
		// this._handled should return false once, and after return true.
		this._handled = returnTrue;
		return false;
	}

	function flagAsHandled(e) {
		e._handled();
	}

	function triggerStart(e, template, touch, distX, distY, fn) {
		var node = template.target,
		    touches, time;

		touches = e.targetTouches;
		time = e.timeStamp - template.timeStamp;

		// Create a movestart object with some special properties that
		// are passed only to the movestart handlers.
		template.type = 'movestart';
		template.distX = distX;
		template.distY = distY;
		template.deltaX = distX;
		template.deltaY = distY;
		template.pageX = touch.pageX;
		template.pageY = touch.pageY;
		template.velocityX = distX / time;
		template.velocityY = distY / time;
		template.targetTouches = touches;
		template.finger = touches ?
			touches.length :
			1 ;

		// The _handled method is fired to tell the default movestart
		// handler that one of the move events is bound.
		template._handled = handled;

		// Pass the touchmove event so it can be prevented if or when
		// movestart is handled.
		template._preventTouchmoveDefault = function() {
			e.preventDefault();
		};

		// Trigger the movestart event.
		trigger(template.target, template);

		// Unbind handlers that tracked the touch or mouse up till now.
		fn(template.identifier);
	}


	// Handlers that control what happens following a movestart

	function activeMousemove(e) {
		var timer = e.data.timer;

		e.data.touch = e;
		e.data.timeStamp = e.timeStamp;
		timer.kick();
	}

	function activeMouseend(e) {
		var event = e.data.event,
		    timer = e.data.timer;

		removeActiveMouse();

		endEvent(event, timer, function() {
			// Unbind the click suppressor, waiting until after mouseup
			// has been handled.
			setTimeout(function(){
				remove(event.target, 'click', returnFalse);
			}, 0);
		});
	}

	function removeActiveMouse(event) {
		remove(document, mouseevents.move, activeMousemove);
		remove(document, mouseevents.end, activeMouseend);
	}

	function activeTouchmove(e) {
		var event = e.data.event,
		    timer = e.data.timer,
		    touch = changedTouch(e, event);

		if (!touch) { return; }

		// Stop the interface from gesturing
		e.preventDefault();

		event.targetTouches = e.targetTouches;
		e.data.touch = touch;
		e.data.timeStamp = e.timeStamp;
		timer.kick();
	}

	function activeTouchend(e) {
		var event = e.data.event,
		    timer = e.data.timer,
		    touch = identifiedTouch(e.changedTouches, event.identifier);

		// This isn't the touch you're looking for.
		if (!touch) { return; }

		removeActiveTouch(event);
		endEvent(event, timer);
	}

	function removeActiveTouch(event) {
		remove(document, '.' + event.identifier, activeTouchmove);
		remove(document, '.' + event.identifier, activeTouchend);
	}


	// Logic for triggering move and moveend events

	function updateEvent(event, touch, timeStamp, timer) {
		var time = timeStamp - event.timeStamp;

		event.type = 'move';
		event.distX =  touch.pageX - event.startX;
		event.distY =  touch.pageY - event.startY;
		event.deltaX = touch.pageX - event.pageX;
		event.deltaY = touch.pageY - event.pageY;

		// Average the velocity of the last few events using a decay
		// curve to even out spurious jumps in values.
		event.velocityX = 0.3 * event.velocityX + 0.7 * event.deltaX / time;
		event.velocityY = 0.3 * event.velocityY + 0.7 * event.deltaY / time;
		event.pageX =  touch.pageX;
		event.pageY =  touch.pageY;
	}

	function endEvent(event, timer, fn) {
		timer.end(function(){
			event.type = 'moveend';

			trigger(event.target, event);

			return fn && fn();
		});
	}


	// jQuery special event definition

	function setup(data, namespaces, eventHandle) {
		// Stop the node from being dragged
		//add(this, 'dragstart.move drag.move', preventDefault);

		// Prevent text selection and touch interface scrolling
		//add(this, 'mousedown.move', preventIgnoreTags);

		// Tell movestart default handler that we've handled this
		add(this, 'movestart.move', flagAsHandled);

		// Don't bind to the DOM. For speed.
		return true;
	}

	function teardown(namespaces) {
		remove(this, 'dragstart drag', preventDefault);
		remove(this, 'mousedown touchstart', preventIgnoreTags);
		remove(this, 'movestart', flagAsHandled);

		// Don't bind to the DOM. For speed.
		return true;
	}

	function addMethod(handleObj) {
		// We're not interested in preventing defaults for handlers that
		// come from internal move or moveend bindings
		if (handleObj.namespace === "move" || handleObj.namespace === "moveend") {
			return;
		}

		// Stop the node from being dragged
		add(this, 'dragstart.' + handleObj.guid + ' drag.' + handleObj.guid, preventDefault, undefined, handleObj.selector);

		// Prevent text selection and touch interface scrolling
		add(this, 'mousedown.' + handleObj.guid, preventIgnoreTags, undefined, handleObj.selector);
	}

	function removeMethod(handleObj) {
		if (handleObj.namespace === "move" || handleObj.namespace === "moveend") {
			return;
		}

		remove(this, 'dragstart.' + handleObj.guid + ' drag.' + handleObj.guid);
		remove(this, 'mousedown.' + handleObj.guid);
	}

	jQuery.event.special.movestart = {
		setup: setup,
		teardown: teardown,
		add: addMethod,
		remove: removeMethod,

		_default: function(e) {
			var event, data;

			// If no move events were bound to any ancestors of this
			// target, high tail it out of here.
			if (!e._handled()) { return; }

			function update(time) {
				updateEvent(event, data.touch, data.timeStamp);
				trigger(e.target, event);
			}

			event = {
				target: e.target,
				startX: e.startX,
				startY: e.startY,
				pageX: e.pageX,
				pageY: e.pageY,
				distX: e.distX,
				distY: e.distY,
				deltaX: e.deltaX,
				deltaY: e.deltaY,
				velocityX: e.velocityX,
				velocityY: e.velocityY,
				timeStamp: e.timeStamp,
				identifier: e.identifier,
				targetTouches: e.targetTouches,
				finger: e.finger
			};

			data = {
				event: event,
				timer: new Timer(update),
				touch: undefined,
				timeStamp: undefined
			};

			if (e.identifier === undefined) {
				// We're dealing with a mouse
				// Stop clicks from propagating during a move
				add(e.target, 'click', returnFalse);
				add(document, mouseevents.move, activeMousemove, data);
				add(document, mouseevents.end, activeMouseend, data);
			}
			else {
				// We're dealing with a touch. Stop touchmove doing
				// anything defaulty.
				e._preventTouchmoveDefault();
				add(document, touchevents.move + '.' + e.identifier, activeTouchmove, data);
				add(document, touchevents.end + '.' + e.identifier, activeTouchend, data);
			}
		}
	};

	jQuery.event.special.move = {
		setup: function() {
			// Bind a noop to movestart. Why? It's the movestart
			// setup that decides whether other move events are fired.
			add(this, 'movestart.move', jQuery.noop);
		},

		teardown: function() {
			remove(this, 'movestart.move', jQuery.noop);
		}
	};

	jQuery.event.special.moveend = {
		setup: function() {
			// Bind a noop to movestart. Why? It's the movestart
			// setup that decides whether other move events are fired.
			add(this, 'movestart.moveend', jQuery.noop);
		},

		teardown: function() {
			remove(this, 'movestart.moveend', jQuery.noop);
		}
	};

	add(document, 'mousedown.move', mousedown);
	add(document, 'touchstart.move', touchstart);

	// Make jQuery copy touch event properties over to the jQuery event
	// object, if they are not already listed. But only do the ones we
	// really need. IE7/8 do not have Array#indexOf(), but nor do they
	// have touch events, so let's assume we can ignore them.
	if (typeof Array.prototype.indexOf === 'function') {
		(function(jQuery, undefined){
			var props = ["changedTouches", "targetTouches"],
			    l = props.length;

			while (l--) {
				if (jQuery.event.props.indexOf(props[l]) === -1) {
					jQuery.event.props.push(props[l]);
				}
			}
		})(jQuery);
	};
});

// jQuery.event.swipe
// 0.5
// Stephen Band

// Dependencies
// jQuery.event.move 1.2

// One of swipeleft, swiperight, swipeup or swipedown is triggered on
// moveend, when the move has covered a threshold ratio of the dimension
// of the target node, or has gone really fast. Threshold and velocity
// sensitivity changed with:
//
// jQuery.event.special.swipe.settings.threshold
// jQuery.event.special.swipe.settings.sensitivity

(function (module) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register as an anonymous module.
		define(['jquery'], module);
	} else {
		// Browser globals
		module(jQuery);
	}
})(function(jQuery, undefined){
	var add = jQuery.event.add,

	    remove = jQuery.event.remove,

	    // Just sugar, so we can have arguments in the same order as
	    // add and remove.
	    trigger = function(node, type, data) {
	    	jQuery.event.trigger(type, data, node);
	    },

	    settings = {
	    	// Ratio of distance over target finger must travel to be
	    	// considered a swipe.
	    	threshold: 0.4,
	    	// Faster fingers can travel shorter distances to be considered
	    	// swipes. 'sensitivity' controls how much. Bigger is shorter.
	    	sensitivity: 6
	    };

	function moveend(e) {
		var w, h, event;

		w = e.currentTarget.offsetWidth;
		h = e.currentTarget.offsetHeight;

		// Copy over some useful properties from the move event
		event = {
			distX: e.distX,
			distY: e.distY,
			velocityX: e.velocityX,
			velocityY: e.velocityY,
			finger: e.finger
		};

		// Find out which of the four directions was swiped
		if (e.distX > e.distY) {
			if (e.distX > -e.distY) {
				if (e.distX/w > settings.threshold || e.velocityX * e.distX/w * settings.sensitivity > 1) {
					event.type = 'swiperight';
					trigger(e.currentTarget, event);
				}
			}
			else {
				if (-e.distY/h > settings.threshold || e.velocityY * e.distY/w * settings.sensitivity > 1) {
					event.type = 'swipeup';
					trigger(e.currentTarget, event);
				}
			}
		}
		else {
			if (e.distX > -e.distY) {
				if (e.distY/h > settings.threshold || e.velocityY * e.distY/w * settings.sensitivity > 1) {
					event.type = 'swipedown';
					trigger(e.currentTarget, event);
				}
			}
			else {
				if (-e.distX/w > settings.threshold || e.velocityX * e.distX/w * settings.sensitivity > 1) {
					event.type = 'swipeleft';
					trigger(e.currentTarget, event);
				}
			}
		}
	}

	function getData(node) {
		var data = jQuery.data(node, 'event_swipe');

		if (!data) {
			data = { count: 0 };
			jQuery.data(node, 'event_swipe', data);
		}

		return data;
	}

	jQuery.event.special.swipe =
	jQuery.event.special.swipeleft =
	jQuery.event.special.swiperight =
	jQuery.event.special.swipeup =
	jQuery.event.special.swipedown = {
		setup: function( data, namespaces, eventHandle ) {
			var data = getData(this);

			// If another swipe event is already setup, don't setup again.
			if (data.count++ > 0) { return; }

			add(this, 'moveend', moveend);

			return true;
		},

		teardown: function() {
			var data = getData(this);

			// If another swipe event is still setup, don't teardown.
			if (--data.count > 0) { return; }

			remove(this, 'moveend', moveend);

			return true;
		},

		settings: settings
	};
});

//     keymaster.js
//     (c) 2011-2013 Thomas Fuchs
//     keymaster.js may be freely distributed under the MIT license.

;(function(global){
  var k,
    _handlers = {},
    _mods = { 16: false, 18: false, 17: false, 91: false },
    _scope = 'all',
    // modifier keys
    _MODIFIERS = {
      '⇧': 16, shift: 16,
      '⌥': 18, alt: 18, option: 18,
      '⌃': 17, ctrl: 17, control: 17,
      '⌘': 91, command: 91
    },
    // special keys
    _MAP = {
      backspace: 8, tab: 9, clear: 12,
      enter: 13, 'return': 13,
      esc: 27, escape: 27, space: 32,
      left: 37, up: 38,
      right: 39, down: 40,
      del: 46, 'delete': 46,
      home: 36, end: 35,
      pageup: 33, pagedown: 34,
      ',': 188, '.': 190, '/': 191,
      '`': 192, '-': 189, '=': 187,
      ';': 186, '\'': 222,
      '[': 219, ']': 221, '\\': 220
    },
    code = function(x){
      return _MAP[x] || x.toUpperCase().charCodeAt(0);
    },
    _downKeys = [];

  for(k=1;k<20;k++) _MAP['f'+k] = 111+k;

  // IE doesn't support Array#indexOf, so have a simple replacement
  function index(array, item){
    var i = array.length;
    while(i--) if(array[i]===item) return i;
    return -1;
  }

  // for comparing mods before unassignment
  function compareArray(a1, a2) {
    if (a1.length != a2.length) return false;
    for (var i = 0; i < a1.length; i++) {
        if (a1[i] !== a2[i]) return false;
    }
    return true;
  }

  var modifierMap = {
      16:'shiftKey',
      18:'altKey',
      17:'ctrlKey',
      91:'metaKey'
  };
  function updateModifierKey(event) {
      for(k in _mods) _mods[k] = event[modifierMap[k]];
  };

  // handle keydown event
  function dispatch(event) {
    var key, handler, k, i, modifiersMatch, scope;
    key = event.keyCode;

    if (index(_downKeys, key) == -1) {
        _downKeys.push(key);
    }

    // if a modifier key, set the key.<modifierkeyname> property to true and return
    if(key == 93 || key == 224) key = 91; // right command on webkit, command on Gecko
    if(key in _mods) {
      _mods[key] = true;
      // 'assignKey' from inside this closure is exported to window.key
      for(k in _MODIFIERS) if(_MODIFIERS[k] == key) assignKey[k] = true;
      return;
    }
    updateModifierKey(event);

    // see if we need to ignore the keypress (filter() can can be overridden)
    // by default ignore key presses if a select, textarea, or input is focused
    if(!assignKey.filter.call(this, event)) return;

    // abort if no potentially matching shortcuts found
    if (!(key in _handlers)) return;

    scope = getScope();

    // for each potential shortcut
    for (i = 0; i < _handlers[key].length; i++) {
      handler = _handlers[key][i];

      // see if it's in the current scope
      if(handler.scope == scope || handler.scope == 'all'){
        // check if modifiers match if any
        modifiersMatch = handler.mods.length > 0;
        for(k in _mods)
          if((!_mods[k] && index(handler.mods, +k) > -1) ||
            (_mods[k] && index(handler.mods, +k) == -1)) modifiersMatch = false;
        // call the handler and stop the event if neccessary
        if((handler.mods.length == 0 && !_mods[16] && !_mods[18] && !_mods[17] && !_mods[91]) || modifiersMatch){
          if(handler.method(event, handler)===false){
            if(event.preventDefault) event.preventDefault();
              else event.returnValue = false;
            if(event.stopPropagation) event.stopPropagation();
            if(event.cancelBubble) event.cancelBubble = true;
          }
        }
      }
    }
  };

  // unset modifier keys on keyup
  function clearModifier(event){
    var key = event.keyCode, k,
        i = index(_downKeys, key);

    // remove key from _downKeys
    if (i >= 0) {
        _downKeys.splice(i, 1);
    }

    if(key == 93 || key == 224) key = 91;
    if(key in _mods) {
      _mods[key] = false;
      for(k in _MODIFIERS) if(_MODIFIERS[k] == key) assignKey[k] = false;
    }
  };

  function resetModifiers() {
    for(k in _mods) _mods[k] = false;
    for(k in _MODIFIERS) assignKey[k] = false;
  };

  // parse and assign shortcut
  function assignKey(key, scope, method){
    var keys, mods;
    keys = getKeys(key);
    if (method === undefined) {
      method = scope;
      scope = 'all';
    }

    // for each shortcut
    for (var i = 0; i < keys.length; i++) {
      // set modifier keys if any
      mods = [];
      key = keys[i].split('+');
      if (key.length > 1){
        mods = getMods(key);
        key = [key[key.length-1]];
      }
      // convert to keycode and...
      key = key[0]
      key = code(key);
      // ...store handler
      if (!(key in _handlers)) _handlers[key] = [];
      _handlers[key].push({ shortcut: keys[i], scope: scope, method: method, key: keys[i], mods: mods });
    }
  };

  // unbind all handlers for given key in current scope
  function unbindKey(key, scope) {
    var multipleKeys, keys,
      mods = [],
      i, j, obj;

    multipleKeys = getKeys(key);

    for (j = 0; j < multipleKeys.length; j++) {
      keys = multipleKeys[j].split('+');

      if (keys.length > 1) {
        mods = getMods(keys);
      }

      key = keys[keys.length - 1];
      key = code(key);

      if (scope === undefined) {
        scope = getScope();
      }
      if (!_handlers[key]) {
        return;
      }
      for (i = 0; i < _handlers[key].length; i++) {
        obj = _handlers[key][i];
        // only clear handlers if correct scope and mods match
        if (obj.scope === scope && compareArray(obj.mods, mods)) {
          _handlers[key][i] = {};
        }
      }
    }
  };

  // Returns true if the key with code 'keyCode' is currently down
  // Converts strings into key codes.
  function isPressed(keyCode) {
      if (typeof(keyCode)=='string') {
        keyCode = code(keyCode);
      }
      return index(_downKeys, keyCode) != -1;
  }

  function getPressedKeyCodes() {
      return _downKeys.slice(0);
  }

  function filter(event){
    var tagName = (event.target || event.srcElement).tagName;
    // ignore keypressed in any elements that support keyboard data input
    return !(tagName == 'INPUT' || tagName == 'SELECT' || tagName == 'TEXTAREA');
  }

  // initialize key.<modifier> to false
  for(k in _MODIFIERS) assignKey[k] = false;

  // set current scope (default 'all')
  function setScope(scope){ _scope = scope || 'all' };
  function getScope(){ return _scope || 'all' };

  // delete all handlers for a given scope
  function deleteScope(scope){
    var key, handlers, i;

    for (key in _handlers) {
      handlers = _handlers[key];
      for (i = 0; i < handlers.length; ) {
        if (handlers[i].scope === scope) handlers.splice(i, 1);
        else i++;
      }
    }
  };

  // abstract key logic for assign and unassign
  function getKeys(key) {
    var keys;
    key = key.replace(/\s/g, '');
    keys = key.split(',');
    if ((keys[keys.length - 1]) == '') {
      keys[keys.length - 2] += ',';
    }
    return keys;
  }

  // abstract mods logic for assign and unassign
  function getMods(key) {
    var mods = key.slice(0, key.length - 1);
    for (var mi = 0; mi < mods.length; mi++)
    mods[mi] = _MODIFIERS[mods[mi]];
    return mods;
  }

  // cross-browser events
  function addEvent(object, event, method) {
    if (object.addEventListener)
      object.addEventListener(event, method, false);
    else if(object.attachEvent)
      object.attachEvent('on'+event, function(){ method(window.event) });
  };

  // set the handlers globally on document
  addEvent(document, 'keydown', function(event) { dispatch(event) }); // Passing _scope to a callback to ensure it remains the same by execution. Fixes #48
  addEvent(document, 'keyup', clearModifier);

  // reset modifiers to false whenever the window is (re)focused.
  addEvent(window, 'focus', resetModifiers);

  // store previously defined key
  var previousKey = global.key;

  // restore previously defined key and return reference to our key object
  function noConflict() {
    var k = global.key;
    global.key = previousKey;
    return k;
  }

  // set window.key and window.key.set/get/deleteScope, and the default filter
  global.key = assignKey;
  global.key.setScope = setScope;
  global.key.getScope = getScope;
  global.key.deleteScope = deleteScope;
  global.key.filter = filter;
  global.key.isPressed = isPressed;
  global.key.getPressedKeyCodes = getPressedKeyCodes;
  global.key.noConflict = noConflict;
  global.key.unbind = unbindKey;

  if(typeof module !== 'undefined') module.exports = assignKey;

})(this);

/*

StackBlur - a fast almost Gaussian Blur For Canvas

Version: 	0.5
Author:		Mario Klingemann
Contact: 	mario@quasimondo.com
Website:	http://www.quasimondo.com/StackBlurForCanvas
Twitter:	@quasimondo

In case you find this class useful - especially in commercial projects -
I am not totally unhappy for a small donation to my PayPal account
mario@quasimondo.de

Or support me on flattr: 
https://flattr.com/thing/72791/StackBlur-a-fast-almost-Gaussian-Blur-Effect-for-CanvasJavascript

Copyright (c) 2010 Mario Klingemann

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

var mul_table = [
        512,512,456,512,328,456,335,512,405,328,271,456,388,335,292,512,
        454,405,364,328,298,271,496,456,420,388,360,335,312,292,273,512,
        482,454,428,405,383,364,345,328,312,298,284,271,259,496,475,456,
        437,420,404,388,374,360,347,335,323,312,302,292,282,273,265,512,
        497,482,468,454,441,428,417,405,394,383,373,364,354,345,337,328,
        320,312,305,298,291,284,278,271,265,259,507,496,485,475,465,456,
        446,437,428,420,412,404,396,388,381,374,367,360,354,347,341,335,
        329,323,318,312,307,302,297,292,287,282,278,273,269,265,261,512,
        505,497,489,482,475,468,461,454,447,441,435,428,422,417,411,405,
        399,394,389,383,378,373,368,364,359,354,350,345,341,337,332,328,
        324,320,316,312,309,305,301,298,294,291,287,284,281,278,274,271,
        268,265,262,259,257,507,501,496,491,485,480,475,470,465,460,456,
        451,446,442,437,433,428,424,420,416,412,408,404,400,396,392,388,
        385,381,377,374,370,367,363,360,357,354,350,347,344,341,338,335,
        332,329,326,323,320,318,315,312,310,307,304,302,299,297,294,292,
        289,287,285,282,280,278,275,273,271,269,267,265,263,261,259];
        
   
var shg_table = [
	     9, 11, 12, 13, 13, 14, 14, 15, 15, 15, 15, 16, 16, 16, 16, 17, 
		17, 17, 17, 17, 17, 17, 18, 18, 18, 18, 18, 18, 18, 18, 18, 19, 
		19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 20, 20, 20,
		20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 21,
		21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
		21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 22, 22, 22, 22, 22, 22, 
		22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22,
		22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 23, 
		23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23,
		23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23,
		23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 
		23, 23, 23, 23, 23, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 
		24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
		24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
		24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
		24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24 ];

function stackBlurImage( img, canvasID, radius, blurAlphaChannel )
{
			
 	//var img = document.getElementById( imageID );
	var w = img.width;
    var h = img.height;
       
	var canvas = document.getElementById( canvasID );
      
    canvas.style.width  = w + "px";
    canvas.style.height = h + "px";
    canvas.width = w;
    canvas.height = h;
    
    var context = canvas.getContext("2d");
    context.clearRect( 0, 0, w, h );
    context.drawImage( img, 0, 0 );

	if ( isNaN(radius) || radius < 1 ) return;
	
	if ( blurAlphaChannel )
		stackBlurCanvasRGBA( canvasID, 0, 0, w, h, radius );
	else 
		stackBlurCanvasRGB( canvasID, 0, 0, w, h, radius );
}


function stackBlurCanvasRGBA( id, top_x, top_y, width, height, radius )
{
	if ( isNaN(radius) || radius < 1 ) return;
	radius |= 0;
	
	var canvas  = document.getElementById( id );
	var context = canvas.getContext("2d");
	var imageData;
	
	try {
	  try {
		imageData = context.getImageData( top_x, top_y, width, height );
	  } catch(e) {
	  
		// NOTE: this part is supposedly only needed if you want to work with local files
		// so it might be okay to remove the whole try/catch block and just use
		// imageData = context.getImageData( top_x, top_y, width, height );
		try {
			netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
			imageData = context.getImageData( top_x, top_y, width, height );
		} catch(e) {
			console.log("Cannot access local image");
			//throw new Error("unable to access local image data: " + e);
			return;
		}
	  }
	} catch(e) {
	  console.log("Cannot access image");
	  //throw new Error("unable to access image data: " + e);
	}
			
	var pixels = imageData.data;
			
	var x, y, i, p, yp, yi, yw, r_sum, g_sum, b_sum, a_sum, 
	r_out_sum, g_out_sum, b_out_sum, a_out_sum,
	r_in_sum, g_in_sum, b_in_sum, a_in_sum, 
	pr, pg, pb, pa, rbs;
			
	var div = radius + radius + 1;
	var w4 = width << 2;
	var widthMinus1  = width - 1;
	var heightMinus1 = height - 1;
	var radiusPlus1  = radius + 1;
	var sumFactor = radiusPlus1 * ( radiusPlus1 + 1 ) / 2;
	
	var stackStart = new BlurStack();
	var stack = stackStart;
	for ( i = 1; i < div; i++ )
	{
		stack = stack.next = new BlurStack();
		if ( i == radiusPlus1 ) var stackEnd = stack;
	}
	stack.next = stackStart;
	var stackIn = null;
	var stackOut = null;
	
	yw = yi = 0;
	
	var mul_sum = mul_table[radius];
	var shg_sum = shg_table[radius];
	
	for ( y = 0; y < height; y++ )
	{
		r_in_sum = g_in_sum = b_in_sum = a_in_sum = r_sum = g_sum = b_sum = a_sum = 0;
		
		r_out_sum = radiusPlus1 * ( pr = pixels[yi] );
		g_out_sum = radiusPlus1 * ( pg = pixels[yi+1] );
		b_out_sum = radiusPlus1 * ( pb = pixels[yi+2] );
		a_out_sum = radiusPlus1 * ( pa = pixels[yi+3] );
		
		r_sum += sumFactor * pr;
		g_sum += sumFactor * pg;
		b_sum += sumFactor * pb;
		a_sum += sumFactor * pa;
		
		stack = stackStart;
		
		for( i = 0; i < radiusPlus1; i++ )
		{
			stack.r = pr;
			stack.g = pg;
			stack.b = pb;
			stack.a = pa;
			stack = stack.next;
		}
		
		for( i = 1; i < radiusPlus1; i++ )
		{
			p = yi + (( widthMinus1 < i ? widthMinus1 : i ) << 2 );
			r_sum += ( stack.r = ( pr = pixels[p])) * ( rbs = radiusPlus1 - i );
			g_sum += ( stack.g = ( pg = pixels[p+1])) * rbs;
			b_sum += ( stack.b = ( pb = pixels[p+2])) * rbs;
			a_sum += ( stack.a = ( pa = pixels[p+3])) * rbs;
			
			r_in_sum += pr;
			g_in_sum += pg;
			b_in_sum += pb;
			a_in_sum += pa;
			
			stack = stack.next;
		}
		
		
		stackIn = stackStart;
		stackOut = stackEnd;
		for ( x = 0; x < width; x++ )
		{
			pixels[yi+3] = pa = (a_sum * mul_sum) >> shg_sum;
			if ( pa != 0 )
			{
				pa = 255 / pa;
				pixels[yi]   = ((r_sum * mul_sum) >> shg_sum) * pa;
				pixels[yi+1] = ((g_sum * mul_sum) >> shg_sum) * pa;
				pixels[yi+2] = ((b_sum * mul_sum) >> shg_sum) * pa;
			} else {
				pixels[yi] = pixels[yi+1] = pixels[yi+2] = 0;
			}
			
			r_sum -= r_out_sum;
			g_sum -= g_out_sum;
			b_sum -= b_out_sum;
			a_sum -= a_out_sum;
			
			r_out_sum -= stackIn.r;
			g_out_sum -= stackIn.g;
			b_out_sum -= stackIn.b;
			a_out_sum -= stackIn.a;
			
			p =  ( yw + ( ( p = x + radius + 1 ) < widthMinus1 ? p : widthMinus1 ) ) << 2;
			
			r_in_sum += ( stackIn.r = pixels[p]);
			g_in_sum += ( stackIn.g = pixels[p+1]);
			b_in_sum += ( stackIn.b = pixels[p+2]);
			a_in_sum += ( stackIn.a = pixels[p+3]);
			
			r_sum += r_in_sum;
			g_sum += g_in_sum;
			b_sum += b_in_sum;
			a_sum += a_in_sum;
			
			stackIn = stackIn.next;
			
			r_out_sum += ( pr = stackOut.r );
			g_out_sum += ( pg = stackOut.g );
			b_out_sum += ( pb = stackOut.b );
			a_out_sum += ( pa = stackOut.a );
			
			r_in_sum -= pr;
			g_in_sum -= pg;
			b_in_sum -= pb;
			a_in_sum -= pa;
			
			stackOut = stackOut.next;

			yi += 4;
		}
		yw += width;
	}

	
	for ( x = 0; x < width; x++ )
	{
		g_in_sum = b_in_sum = a_in_sum = r_in_sum = g_sum = b_sum = a_sum = r_sum = 0;
		
		yi = x << 2;
		r_out_sum = radiusPlus1 * ( pr = pixels[yi]);
		g_out_sum = radiusPlus1 * ( pg = pixels[yi+1]);
		b_out_sum = radiusPlus1 * ( pb = pixels[yi+2]);
		a_out_sum = radiusPlus1 * ( pa = pixels[yi+3]);
		
		r_sum += sumFactor * pr;
		g_sum += sumFactor * pg;
		b_sum += sumFactor * pb;
		a_sum += sumFactor * pa;
		
		stack = stackStart;
		
		for( i = 0; i < radiusPlus1; i++ )
		{
			stack.r = pr;
			stack.g = pg;
			stack.b = pb;
			stack.a = pa;
			stack = stack.next;
		}
		
		yp = width;
		
		for( i = 1; i <= radius; i++ )
		{
			yi = ( yp + x ) << 2;
			
			r_sum += ( stack.r = ( pr = pixels[yi])) * ( rbs = radiusPlus1 - i );
			g_sum += ( stack.g = ( pg = pixels[yi+1])) * rbs;
			b_sum += ( stack.b = ( pb = pixels[yi+2])) * rbs;
			a_sum += ( stack.a = ( pa = pixels[yi+3])) * rbs;
		   
			r_in_sum += pr;
			g_in_sum += pg;
			b_in_sum += pb;
			a_in_sum += pa;
			
			stack = stack.next;
		
			if( i < heightMinus1 )
			{
				yp += width;
			}
		}
		
		yi = x;
		stackIn = stackStart;
		stackOut = stackEnd;
		for ( y = 0; y < height; y++ )
		{
			p = yi << 2;
			pixels[p+3] = pa = (a_sum * mul_sum) >> shg_sum;
			if ( pa > 0 )
			{
				pa = 255 / pa;
				pixels[p]   = ((r_sum * mul_sum) >> shg_sum ) * pa;
				pixels[p+1] = ((g_sum * mul_sum) >> shg_sum ) * pa;
				pixels[p+2] = ((b_sum * mul_sum) >> shg_sum ) * pa;
			} else {
				pixels[p] = pixels[p+1] = pixels[p+2] = 0;
			}
			
			r_sum -= r_out_sum;
			g_sum -= g_out_sum;
			b_sum -= b_out_sum;
			a_sum -= a_out_sum;
		   
			r_out_sum -= stackIn.r;
			g_out_sum -= stackIn.g;
			b_out_sum -= stackIn.b;
			a_out_sum -= stackIn.a;
			
			p = ( x + (( ( p = y + radiusPlus1) < heightMinus1 ? p : heightMinus1 ) * width )) << 2;
			
			r_sum += ( r_in_sum += ( stackIn.r = pixels[p]));
			g_sum += ( g_in_sum += ( stackIn.g = pixels[p+1]));
			b_sum += ( b_in_sum += ( stackIn.b = pixels[p+2]));
			a_sum += ( a_in_sum += ( stackIn.a = pixels[p+3]));
		   
			stackIn = stackIn.next;
			
			r_out_sum += ( pr = stackOut.r );
			g_out_sum += ( pg = stackOut.g );
			b_out_sum += ( pb = stackOut.b );
			a_out_sum += ( pa = stackOut.a );
			
			r_in_sum -= pr;
			g_in_sum -= pg;
			b_in_sum -= pb;
			a_in_sum -= pa;
			
			stackOut = stackOut.next;
			
			yi += width;
		}
	}
	
	context.putImageData( imageData, top_x, top_y );
	
}


function stackBlurCanvasRGB( id, top_x, top_y, width, height, radius )
{
	if ( isNaN(radius) || radius < 1 ) return;
	radius |= 0;
	
	var canvas  = document.getElementById( id );
	var context = canvas.getContext("2d");
	var imageData;
	
	try {
	  try {
		imageData = context.getImageData( top_x, top_y, width, height );
	  } catch(e) {
	  
		// NOTE: this part is supposedly only needed if you want to work with local files
		// so it might be okay to remove the whole try/catch block and just use
		// imageData = context.getImageData( top_x, top_y, width, height );
		try {
			netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
			imageData = context.getImageData( top_x, top_y, width, height );
		} catch(e) {
			console.log("Cannot access local image");
			//throw new Error("unable to access local image data: " + e);
			return;
		}
	  }
	} catch(e) {
	  console.log("Cannot access image");
	  //throw new Error("unable to access image data: " + e);
	}
			
	var pixels = imageData.data;
			
	var x, y, i, p, yp, yi, yw, r_sum, g_sum, b_sum,
	r_out_sum, g_out_sum, b_out_sum,
	r_in_sum, g_in_sum, b_in_sum,
	pr, pg, pb, rbs;
			
	var div = radius + radius + 1;
	var w4 = width << 2;
	var widthMinus1  = width - 1;
	var heightMinus1 = height - 1;
	var radiusPlus1  = radius + 1;
	var sumFactor = radiusPlus1 * ( radiusPlus1 + 1 ) / 2;
	
	var stackStart = new BlurStack();
	var stack = stackStart;
	for ( i = 1; i < div; i++ )
	{
		stack = stack.next = new BlurStack();
		if ( i == radiusPlus1 ) var stackEnd = stack;
	}
	stack.next = stackStart;
	var stackIn = null;
	var stackOut = null;
	
	yw = yi = 0;
	
	var mul_sum = mul_table[radius];
	var shg_sum = shg_table[radius];
	
	for ( y = 0; y < height; y++ )
	{
		r_in_sum = g_in_sum = b_in_sum = r_sum = g_sum = b_sum = 0;
		
		r_out_sum = radiusPlus1 * ( pr = pixels[yi] );
		g_out_sum = radiusPlus1 * ( pg = pixels[yi+1] );
		b_out_sum = radiusPlus1 * ( pb = pixels[yi+2] );
		
		r_sum += sumFactor * pr;
		g_sum += sumFactor * pg;
		b_sum += sumFactor * pb;
		
		stack = stackStart;
		
		for( i = 0; i < radiusPlus1; i++ )
		{
			stack.r = pr;
			stack.g = pg;
			stack.b = pb;
			stack = stack.next;
		}
		
		for( i = 1; i < radiusPlus1; i++ )
		{
			p = yi + (( widthMinus1 < i ? widthMinus1 : i ) << 2 );
			r_sum += ( stack.r = ( pr = pixels[p])) * ( rbs = radiusPlus1 - i );
			g_sum += ( stack.g = ( pg = pixels[p+1])) * rbs;
			b_sum += ( stack.b = ( pb = pixels[p+2])) * rbs;
			
			r_in_sum += pr;
			g_in_sum += pg;
			b_in_sum += pb;
			
			stack = stack.next;
		}
		
		
		stackIn = stackStart;
		stackOut = stackEnd;
		for ( x = 0; x < width; x++ )
		{
			pixels[yi]   = (r_sum * mul_sum) >> shg_sum;
			pixels[yi+1] = (g_sum * mul_sum) >> shg_sum;
			pixels[yi+2] = (b_sum * mul_sum) >> shg_sum;
			
			r_sum -= r_out_sum;
			g_sum -= g_out_sum;
			b_sum -= b_out_sum;
			
			r_out_sum -= stackIn.r;
			g_out_sum -= stackIn.g;
			b_out_sum -= stackIn.b;
			
			p =  ( yw + ( ( p = x + radius + 1 ) < widthMinus1 ? p : widthMinus1 ) ) << 2;
			
			r_in_sum += ( stackIn.r = pixels[p]);
			g_in_sum += ( stackIn.g = pixels[p+1]);
			b_in_sum += ( stackIn.b = pixels[p+2]);
			
			r_sum += r_in_sum;
			g_sum += g_in_sum;
			b_sum += b_in_sum;
			
			stackIn = stackIn.next;
			
			r_out_sum += ( pr = stackOut.r );
			g_out_sum += ( pg = stackOut.g );
			b_out_sum += ( pb = stackOut.b );
			
			r_in_sum -= pr;
			g_in_sum -= pg;
			b_in_sum -= pb;
			
			stackOut = stackOut.next;

			yi += 4;
		}
		yw += width;
	}

	
	for ( x = 0; x < width; x++ )
	{
		g_in_sum = b_in_sum = r_in_sum = g_sum = b_sum = r_sum = 0;
		
		yi = x << 2;
		r_out_sum = radiusPlus1 * ( pr = pixels[yi]);
		g_out_sum = radiusPlus1 * ( pg = pixels[yi+1]);
		b_out_sum = radiusPlus1 * ( pb = pixels[yi+2]);
		
		r_sum += sumFactor * pr;
		g_sum += sumFactor * pg;
		b_sum += sumFactor * pb;
		
		stack = stackStart;
		
		for( i = 0; i < radiusPlus1; i++ )
		{
			stack.r = pr;
			stack.g = pg;
			stack.b = pb;
			stack = stack.next;
		}
		
		yp = width;
		
		for( i = 1; i <= radius; i++ )
		{
			yi = ( yp + x ) << 2;
			
			r_sum += ( stack.r = ( pr = pixels[yi])) * ( rbs = radiusPlus1 - i );
			g_sum += ( stack.g = ( pg = pixels[yi+1])) * rbs;
			b_sum += ( stack.b = ( pb = pixels[yi+2])) * rbs;
			
			r_in_sum += pr;
			g_in_sum += pg;
			b_in_sum += pb;
			
			stack = stack.next;
		
			if( i < heightMinus1 )
			{
				yp += width;
			}
		}
		
		yi = x;
		stackIn = stackStart;
		stackOut = stackEnd;
		for ( y = 0; y < height; y++ )
		{
			p = yi << 2;
			pixels[p]   = (r_sum * mul_sum) >> shg_sum;
			pixels[p+1] = (g_sum * mul_sum) >> shg_sum;
			pixels[p+2] = (b_sum * mul_sum) >> shg_sum;
			
			r_sum -= r_out_sum;
			g_sum -= g_out_sum;
			b_sum -= b_out_sum;
			
			r_out_sum -= stackIn.r;
			g_out_sum -= stackIn.g;
			b_out_sum -= stackIn.b;
			
			p = ( x + (( ( p = y + radiusPlus1) < heightMinus1 ? p : heightMinus1 ) * width )) << 2;
			
			r_sum += ( r_in_sum += ( stackIn.r = pixels[p]));
			g_sum += ( g_in_sum += ( stackIn.g = pixels[p+1]));
			b_sum += ( b_in_sum += ( stackIn.b = pixels[p+2]));
			
			stackIn = stackIn.next;
			
			r_out_sum += ( pr = stackOut.r );
			g_out_sum += ( pg = stackOut.g );
			b_out_sum += ( pb = stackOut.b );
			
			r_in_sum -= pr;
			g_in_sum -= pg;
			b_in_sum -= pb;
			
			stackOut = stackOut.next;
			
			yi += width;
		}
	}
	
	context.putImageData( imageData, top_x, top_y );
	
}

function BlurStack()
{
	this.r = 0;
	this.g = 0;
	this.b = 0;
	this.a = 0;
	this.next = null;
}
var ArrayProto, nativeForEach, nativeIsArray, nativeMap, nativeReduce,
  __hasProp = {}.hasOwnProperty;

if (typeof _ === "undefined" || _ === null) {
  this._ = {};
  ArrayProto = Array.prototype;
  nativeForEach = ArrayProto.forEach;
  nativeMap = ArrayProto.map;
  nativeReduce = ArrayProto.reduce;
  nativeIsArray = Array.isArray;
  _.isObject = function(obj) {
    var type;
    type = typeof obj;
    return type === "function" || type === "object" && !!obj;
  };
  _.after = function(times, func) {
    return function() {
      if (--times < 1) {
        return func.apply(this, arguments);
      }
    };
  };
  _.each = function(obj, iterator, context) {
    var e, i, key, val, _i, _ref;
    try {
      if (nativeForEach && obj.forEach === nativeForEach) {
        obj.forEach(iterator, context);
      } else if (_.isNumber(obj.length)) {
        for (i = _i = 0, _ref = obj.length; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
          iterator.call(context, obj[i], i, obj);
        }
      } else {
        for (key in obj) {
          if (!__hasProp.call(obj, key)) continue;
          val = obj[key];
          iterator.call(context, val, key, obj);
        }
      }
    } catch (_error) {
      e = _error;
    }
    return obj;
  };
  _.map = function(obj, iterator, context) {
    var results;
    if (nativeMap && obj.map === nativeMap) {
      return obj.map(iterator, context);
    }
    results = [];
    _.each(obj, function(value, index, list) {
      return results.push(iterator.call(context, value, index, list));
    });
    return results;
  };
  _.reduce = function(obj, iterator, memo, context) {
    if (nativeReduce && obj.reduce === nativeReduce) {
      if (context) {
        iterator = _.bind(iterator, context);
      }
      return obj.reduce(iterator, memo);
    }
    _.each(obj, function(value, index, list) {
      return memo = iterator.call(context, memo, value, index, list);
    });
    return memo;
  };
  _.isArray = nativeIsArray || function(obj) {
    return !!(obj && obj.concat && obj.unshift && !obj.callee);
  };
  _.max = function(obj, iterator, context) {
    var result;
    if (!iterator && _.isArray(obj)) {
      return Math.max.apply(Math, obj);
    }
    result = {
      computed: -Infinity
    };
    _.each(obj, function(value, index, list) {
      var computed;
      computed = iterator ? iterator.call(context, value, index, list) : value;
      return computed >= result.computed && (result = {
        value: value,
        computed: computed
      });
    });
    return result.value;
  };
  _.min = function(obj, iterator, context) {
    var result;
    if (!iterator && _.isArray(obj)) {
      return Math.min.apply(Math, obj);
    }
    result = {
      computed: Infinity
    };
    _.each(obj, function(value, index, list) {
      var computed;
      computed = iterator ? iterator.call(context, value, index, list) : value;
      return computed < result.computed && (result = {
        value: value,
        computed: computed
      });
    });
    return result.value;
  };
  _.now = Date.now || function() {
    return new Date().getTime();
  };
  _.throttle = function(func, wait, options) {
    var args, context, later, previous, result, timeout;
    context = void 0;
    args = void 0;
    result = void 0;
    timeout = null;
    previous = 0;
    options || (options = {});
    later = function() {
      previous = (options.leading === false ? 0 : _.now());
      timeout = null;
      result = func.apply(context, args);
      context = args = null;
    };
    return function() {
      var now, remaining;
      now = _.now();
      if (!previous && options.leading === false) {
        previous = now;
      }
      remaining = wait - (now - previous);
      context = this;
      args = arguments;
      if (remaining <= 0) {
        clearTimeout(timeout);
        timeout = null;
        previous = now;
        result = func.apply(context, args);
        context = args = null;
      } else {
        if (!timeout && options.trailing !== false) {
          timeout = setTimeout(later, remaining);
        }
      }
      return result;
    };
  };
  _.debounce = function(func, wait, immediate) {
    var args, context, later, result, timeout, timestamp;
    timeout = void 0;
    args = void 0;
    context = void 0;
    timestamp = void 0;
    result = void 0;
    later = function() {
      var last;
      last = _.now() - timestamp;
      if (last < wait) {
        timeout = setTimeout(later, wait - last);
      } else {
        timeout = null;
        if (!immediate) {
          result = func.apply(context, args);
          context = args = null;
        }
      }
    };
    return function() {
      var callNow;
      context = this;
      args = arguments;
      timestamp = _.now();
      callNow = immediate && !timeout;
      if (!timeout) {
        timeout = setTimeout(later, wait);
      }
      if (callNow) {
        result = func.apply(context, args);
        context = args = null;
      }
      return result;
    };
  };
}

(function() {
  var _linear_partition, _scrollbar_width,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  this.Chromatic = this.Chromatic || {};

  _linear_partition = (function() {
    var _cache;
    _cache = {};
    return function(seq, k) {
      var ans, i, j, key, l, m, n, q, r, ref, ref1, ref2, ref3, s, solution, table, x, y;
      key = seq.join() + k;
      if (_cache[key]) {
        return _cache[key];
      }
      n = seq.length;
      if (k <= 0) {
        return [];
      }
      if (k > n) {
        return seq.map(function(x) {
          return [x];
        });
      }
      table = (function() {
        var l, ref, results;
        results = [];
        for (y = l = 0, ref = n; 0 <= ref ? l < ref : l > ref; y = 0 <= ref ? ++l : --l) {
          results.push((function() {
            var q, ref1, results1;
            results1 = [];
            for (x = q = 0, ref1 = k; 0 <= ref1 ? q < ref1 : q > ref1; x = 0 <= ref1 ? ++q : --q) {
              results1.push(0);
            }
            return results1;
          })());
        }
        return results;
      })();
      solution = (function() {
        var l, ref, results;
        results = [];
        for (y = l = 0, ref = n - 1; 0 <= ref ? l < ref : l > ref; y = 0 <= ref ? ++l : --l) {
          results.push((function() {
            var q, ref1, results1;
            results1 = [];
            for (x = q = 0, ref1 = k - 1; 0 <= ref1 ? q < ref1 : q > ref1; x = 0 <= ref1 ? ++q : --q) {
              results1.push(0);
            }
            return results1;
          })());
        }
        return results;
      })();
      for (i = l = 0, ref = n; 0 <= ref ? l < ref : l > ref; i = 0 <= ref ? ++l : --l) {
        table[i][0] = seq[i] + (i ? table[i - 1][0] : 0);
      }
      for (j = q = 0, ref1 = k; 0 <= ref1 ? q < ref1 : q > ref1; j = 0 <= ref1 ? ++q : --q) {
        table[0][j] = seq[0];
      }
      for (i = r = 1, ref2 = n; 1 <= ref2 ? r < ref2 : r > ref2; i = 1 <= ref2 ? ++r : --r) {
        for (j = s = 1, ref3 = k; 1 <= ref3 ? s < ref3 : s > ref3; j = 1 <= ref3 ? ++s : --s) {
          m = _.min((function() {
            var ref4, results, t;
            results = [];
            for (x = t = 0, ref4 = i; 0 <= ref4 ? t < ref4 : t > ref4; x = 0 <= ref4 ? ++t : --t) {
              results.push([_.max([table[x][j - 1], table[i][0] - table[x][0]]), x]);
            }
            return results;
          })(), function(o) {
            return o[0];
          });
          table[i][j] = m[0];
          solution[i - 1][j - 1] = m[1];
        }
      }
      n = n - 1;
      k = k - 2;
      ans = [];
      while (k >= 0) {
        ans = [
          (function() {
            var ref4, ref5, results, t;
            results = [];
            for (i = t = ref4 = solution[n - 1][k] + 1, ref5 = n + 1; ref4 <= ref5 ? t < ref5 : t > ref5; i = ref4 <= ref5 ? ++t : --t) {
              results.push(seq[i]);
            }
            return results;
          })()
        ].concat(ans);
        n = solution[n - 1][k];
        k = k - 1;
      }
      return _cache[key] = [
        (function() {
          var ref4, results, t;
          results = [];
          for (i = t = 0, ref4 = n + 1; 0 <= ref4 ? t < ref4 : t > ref4; i = 0 <= ref4 ? ++t : --t) {
            results.push(seq[i]);
          }
          return results;
        })()
      ].concat(ans);
    };
  })();

  _scrollbar_width = (function() {
    var _cache;
    _cache = null;
    return function() {
      var div, w1, w2;
      if (_cache) {
        return _cache;
      }
      div = $("<div style=\"width:50px;height:50px;overflow:hidden;position:absolute;top:-200px;left:-200px;\"><div style=\"height:100px;\"></div></div>");
      $(document.body).append(div);
      w1 = $("div", div).innerWidth();
      div.css("overflow-y", "auto");
      w2 = $("div", div).innerWidth();
      $(div).remove();
      return _cache = w1 - w2;
    };
  })();

  Chromatic.GalleryView = (function() {
    function GalleryView(el, photos, options) {
      this.zoom = bind(this.zoom, this);
      this.layout = bind(this.layout, this);
      this.senddelete = bind(this.senddelete, this);
      this.successcallback = bind(this.successcallback, this);
      this.jsonp_custom = bind(this.jsonp_custom, this);
      this.checkdeletephoto = bind(this.checkdeletephoto, this);
      this.checkloading = bind(this.checkloading, this);
      this.checkloadinglow = bind(this.checkloadinglow, this);
      this.checkoverflow = bind(this.checkoverflow, this);
      this.lazyLoad = bind(this.lazyLoad, this);
      this.loadhighresphoto = bind(this.loadhighresphoto, this);
      this.el = $(el).addClass('chromatic-gallery');
      this.photos = _.map(photos.photolist, function(p) {
        if (_.isObject(p)) {
          return p;
        } else {
          return {
            small: p
          };
        }
      });
      this.option = {
        theme: photos.theme,
        isCard: photos.isCard,
        about: photos.about,
        contact: photos.contact
      };
      window.flickrurlcollection = {};
      this.zoom_view = new Chromatic.ZoomView(null, this.photos, this.option);
      this.photo_views = _.map(this.photos, (function(_this) {
        return function(photo) {
          return new Chromatic.GalleryPhotoView(_this, photo, _this.option);
        };
      })(this));
      this.ideal_height = parseInt(this.el.children().first().css('height'));
      $(window).on('resize', _.debounce(this.layout, 100));
      this.layout();
    }

    GalleryView.prototype.loadhighresphoto = function() {
      var self;
      self = this;
      return _.each(this.photo_views, function(p) {
        return p.loadhighres(self.checkloading);
      });
    };

    GalleryView.prototype.lazyLoad = function() {
      var threshold, viewport_bottom, viewport_top;
      this.count = this.photo_views.length;
      threshold = 1000;
      viewport_top = this.el.scrollTop() - threshold;
      viewport_bottom = (this.el.height() || $(window).height()) + this.el.scrollTop() + threshold;
      return _.each(this.photo_views, (function(_this) {
        return function(photo_view) {
          return photo_view.load(_this.checkloadinglow);
        };
      })(this));
    };

    GalleryView.prototype.checkoverflow = function() {
      var error;
      try {
        return $('#left').css({
          maxWidth: $('.navigation')[0].getBoundingClientRect().width
        });
      } catch (_error) {
        error = _error;
      }
    };

    GalleryView.prototype.checkloadinglow = function() {
      this.count = this.count - 1;
      if (this.count === 0) {
        return this.loadhighresphoto();
      }
    };

    GalleryView.prototype.checkloading = function(loaded, photographerid, photoid, alias, source, photourl) {
      if (loaded === '1') {
        return this.checkdeletephoto(photographerid, photoid, alias, source, photourl);
      } else if (loaded === '2') {
        return this.senddelete(alias, photographerid, photoid);
      }
    };

    GalleryView.prototype.checkdeletephoto = function(photographerid, photoid, alias, source, photourl) {
      var jsonp;
      if (source === 'Flickr') {
        jsonp = this.jsonp_custom(this.successcallback, photourl, alias, photographerid, photoid);
        this.url = 'http://www.flickr.com/services/oembed/?format=json&jsoncallback=' + jsonp + '&url=' + encodeURIComponent(photourl);
        return $.ajax(this.url, {
          type: 'GET',
          dataType: 'JSONP',
          jsonpCallback: 'callback',
          error: (function(_this) {
            return function(jqXHR, textStatus, errorThrown) {
              if (jqXHR.status === 404 || jqXHR.status === 401) {
                return _this.senddelete(alias, photographerid, photoid);
              }
            };
          })(this)
        });
      }
    };

    GalleryView.prototype.jsonp_custom = function(callback, photourl, alias, photographerid, photoid) {
      var callback_name;
      callback_name = 'jsonp_callback_' + Math.floor(Math.random() * 100000);
      window[callback_name] = (function(_this) {
        return function(data) {
          callback(data, photourl, alias, photographerid, photoid);
          return delete window[callback_name];
        };
      })(this);
      return callback_name;
    };

    GalleryView.prototype.successcallback = function(data, photourl, alias, photographerid, photoid) {
      if ((photourl.length > 0 && data && !data.url) || (photourl.substring(0, photourl.length - 5) !== data.url.substring(0, data.url.length - 5))) {
        this.senddelete(alias, photographerid, photoid);
      }
    };

    GalleryView.prototype.senddelete = function(alias, photographerid, photoid) {
      var deleteurl;
      deleteurl = '/api/' + photographerid + '/photos/' + photoid + '/';
      return $.ajax(deleteurl, {
        type: 'DELETE'
      });
    };

    GalleryView.prototype.layout = function() {
      var column, ideal_height, ideal_width, index, left, maxHeight, partition, rows, summed_width, viewport_width, weights, widthapprox;
      $(document.body).css('overflowY', 'scroll');
      viewport_width = this.el[0].getBoundingClientRect().width - parseInt(this.el.css('paddingLeft')) - parseInt(this.el.css('paddingRight'));
      if (this.el[0].offsetWidth > this.el[0].scrollWidth) {
        viewport_width = viewport_width - _scrollbar_width();
      }
      $(document.body).css('overflowY', 'auto');
      if (this.option.isCard) {
        ideal_width = 300;
        column = Math.round(viewport_width / ideal_width);
        widthapprox = Math.floor(viewport_width / column);
        if (column < 1) {
          _.each(this.photos, (function(_this) {
            return function(photo, i) {
              return _this.photo_views[i].resize(parseInt(ideal_height * photo.aspect_ratio), ideal_height);
            };
          })(this));
        } else {
          weights = _.map(this.photos, function(p) {
            return parseInt(100 / p.aspect_ratio);
          });
          partition = _linear_partition(weights, column);
          index = 0;
          left = 0;
          maxHeight = 0;
          _.each(partition, (function(_this) {
            return function(column) {
              var row_buffer, top;
              row_buffer = [];
              top = 0;
              _.each(column, function(p, i) {
                return row_buffer.push(_this.photos[index + i]);
              });
              _.each(row_buffer, function(p, i) {
                var height, width;
                width = widthapprox;
                height = width / _this.photo_views[index + i].photo.aspect_ratio;
                _this.photo_views[index + i].resize(width, height);
                _this.photo_views[index + i].setposition(left, top);
                return top = top + height;
              });
              if (top >= maxHeight) {
                maxHeight = top;
              }
              index += column.length;
              return left = left + widthapprox;
            };
          })(this));
          this.el.css({
            height: maxHeight
          });
        }
      } else {
        ideal_height = this.ideal_height || parseInt((this.el.height() || $(window).width()) / 2);
        summed_width = _.reduce(this.photos, (function(sum, p) {
          return sum += p.aspect_ratio * ideal_height;
        }), 0);
        rows = Math.round(summed_width / viewport_width);
        if (rows < 1) {
          _.each(this.photos, (function(_this) {
            return function(photo, i) {
              return _this.photo_views[i].resize(parseInt(ideal_height * photo.aspect_ratio), ideal_height);
            };
          })(this));
        } else {
          weights = _.map(this.photos, function(p) {
            return parseInt(p.aspect_ratio * 100);
          });
          partition = _linear_partition(weights, rows);
          index = 0;
          _.each(partition, (function(_this) {
            return function(row) {
              var row_buffer, summed_ars;
              row_buffer = [];
              _.each(row, function(p, i) {
                return row_buffer.push(_this.photos[index + i]);
              });
              summed_ars = _.reduce(row_buffer, (function(sum, p) {
                return sum += p.aspect_ratio;
              }), 0);
              summed_width = 0;
              _.each(row_buffer, function(p, i) {
                var height, width;
                width = i === row_buffer.length - 1 ? viewport_width - summed_width : parseInt(viewport_width / summed_ars * p.aspect_ratio);
                height = parseInt(viewport_width / summed_ars);
                _this.photo_views[index + i].resize(width, height);
                return summed_width += width;
              });
              return index += row.length;
            };
          })(this));
        }
      }
      this.lazyLoad();
      return this.checkoverflow();
    };

    GalleryView.prototype.zoom = function(photo) {
      return this.zoom_view.show(photo);
    };

    return GalleryView;

  })();

}).call(this);

(function() {
  var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  this.Chromatic = this.Chromatic || {};

  Chromatic.GalleryPhotoView = (function() {
    function GalleryPhotoView(parent, photo, options) {
      this.rightclick = bind(this.rightclick, this);
      this.zoom = bind(this.zoom, this);
      this.unload = bind(this.unload, this);
      this.loadhighres = bind(this.loadhighres, this);
      this.load = bind(this.load, this);
      this.parent = parent;
      this.photo = photo;
      this.theme = options.theme;
      this.isCard = options.isCard;
      this.title = photo.title;
      this.el = $('<div class="chromatic-gallery-photo"></div>');
      if (this.isCard) {
        this.el.addClass('chromatic-card');
      }
      this.elimage = $('<img id="' + this.photo.id + 'image" class="chromatic-gallery-photo-image"></img>');
      this.elcanvas = $('<canvas class="chromatic-gallery-photo-image" id="' + this.photo.id + '"></canvas>');
      this.el.on('contextmenu', this.rightclick);
      parent.el.append(this.el);
      this.el.on('click', this.zoom);
    }

    GalleryPhotoView.prototype.load = function(callback) {
      var image;
      if (this.loaded) {
        return callback();
      }
      this.colorList = ['#f2f4ff', '#f2fffd', '#f3f9ff', '#f2fcff', '#f2fff8', '#f0f6fc', '#f6fff2'];
      this.colorListTheme = ['#382d2d', '#252f31', '#343833', '#313236', '#373237'];
      if (this.theme) {
        this.index = this.photo.key % 5;
        this.color = this.colorListTheme[this.index];
      } else {
        this.index = this.photo.key % 7;
        this.color = this.colorList[this.index];
      }
      this.el.css('background-color', this.color);
      image = new Image();
      image.crossOrigin = 'Anonymous';
      image.onload = (function(_this) {
        return function() {
          _this.photo.aspect_ratio = image.width / image.height;
          _this.photo.width = image.width;
          _this.photo.height = image.height;
          callback();
          _this.elimage.attr('src', _this.photo.low);
          _this.el.append(_this.elimage);
          _this.elimage.addClass('loaded-canvas');
          _this.el.append(_this.elcanvas);
          stackBlurImage(image, _this.photo.id, 30, false);
          return _this.loaded = true;
        };
      })(this);
      image.onerror = (function(_this) {
        return function() {
          return callback();
        };
      })(this);
      if (typeof this.photo.low !== 'undefined' && this.photo.source === 'px') {
        return image.src = 'https://farm6.staticflickr.com/5060/5444250007_1392c5bf6d_s.jpg';
      } else if (typeof this.photo.low !== 'undefined') {
        return image.src = this.photo.low;
      } else {
        return image.src = this.photo.small;
      }
    };

    GalleryPhotoView.prototype.loadhighres = function(callback) {
      var image;
      if (this.loaded && !this.photo.small) {
        return callback('0', this.photo.photographerid, this.photo.id, this.photo.alias, this.photo.source, this.photo.low);
      }
      image = new Image();
      image.onload = (function(_this) {
        return function() {
          _this.photo.aspect_ratio = image.width / image.height;
          _this.photo.width = image.width;
          _this.photo.height = image.height;
          callback('1', _this.photo.photographerid, _this.photo.id, _this.photo.alias, _this.photo.source, _this.photo.low);
          _this.elimage.attr('src', _this.photo.small);
          _this.elcanvas.addClass('loaded-canvas');
          _this.elimage.addClass('loaded-image');
          return _this.loaded = true;
        };
      })(this);
      image.onerror = (function(_this) {
        return function() {
          if (callback) {
            return callback('2', _this.photo.photographerid, _this.photo.id, _this.photo.alias, _this.photo.source, _this.photo.low);
          }
        };
      })(this);
      return image.src = this.photo.small;
    };

    GalleryPhotoView.prototype.unload = function() {
      this.el.css('backgroundImage', "");
      this.el.remove();
      return this.loaded = false;
    };

    GalleryPhotoView.prototype.zoom = function() {
      this.parent.zoom(this.photo);
      return this.parent.el.click();
    };

    GalleryPhotoView.prototype.resize = function(width, height) {
      this.el.css({
        width: width - parseInt(this.el.css('marginLeft')) - parseInt(this.el.css('marginRight')),
        height: height - parseInt(this.el.css('marginTop')) - parseInt(this.el.css('marginBottom'))
      });
      this.top = this.el.position().top;
      return this.bottom = this.top + this.el.height();
    };

    GalleryPhotoView.prototype.setposition = function(left, top) {
      return this.el.css({
        top: top,
        left: left
      });
    };

    GalleryPhotoView.prototype.rightclick = function() {
      return false;
    };

    return GalleryPhotoView;

  })();

}).call(this);

(function() {
  var _scrollbar_width,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  this.Chromatic = this.Chromatic || {};

  $.extend($.easing, {
    easeOutCirc: function(x, t, b, c, d) {
      return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
    }
  });

  _scrollbar_width = (function() {
    var _cache;
    _cache = null;
    return function() {
      var div, w1, w2;
      if (_cache) {
        return _cache;
      }
      div = $("<div style=\"width:50px;height:50px;overflow:hidden;position:absolute;top:-200px;left:-200px;\"><div style=\"height:100px;\"></div></div>");
      $(document.body).append(div);
      w1 = $("div", div).innerWidth();
      div.css("overflow-y", "auto");
      w2 = $("div", div).innerWidth();
      $(div).remove();
      return _cache = w1 - w2;
    };
  })();

  Chromatic.ZoomView = (function() {
    function ZoomView(el, photos, options) {
      this.showUrl = bind(this.showUrl, this);
      this.rightclick = bind(this.rightclick, this);
      this.attachclick = bind(this.attachclick, this);
      this.layout = bind(this.layout, this);
      this.setheight = bind(this.setheight, this);
      this.setExifData = bind(this.setExifData, this);
      this.bindExif = bind(this.bindExif, this);
      this.bindData = bind(this.bindData, this);
      this.showPrevious = bind(this.showPrevious, this);
      this.showNext = bind(this.showNext, this);
      this.show = bind(this.show, this);
      this.close = bind(this.close, this);
      this.portfolio = bind(this.portfolio, this);
      this.mousemovement = bind(this.mousemovement, this);
      this.detachTimer = bind(this.detachTimer, this);
      this.attachTimer = bind(this.attachTimer, this);
      this.attachhideInformationEvent = bind(this.attachhideInformationEvent, this);
      this.hidefirstTime = bind(this.hidefirstTime, this);
      this.hideextrainfo = bind(this.hideextrainfo, this);
      this.startTimer = bind(this.startTimer, this);
      this.noExif = bind(this.noExif, this);
      this.hideExif = bind(this.hideExif, this);
      this.showExif = bind(this.showExif, this);
      this.initExif = bind(this.initExif, this);
      this.container = el;
      this.el = $('<div class="chromatic-zoom"/>');
      this.leftarrow = $('<div class=\"chromatic-zoom-arrow-left\"></div>');
      this.rightarrow = $('<div class=\"chromatic-zoom-arrow-right\"></div>');
      this.grid = $('<div class=\"chromatic-zoom-photo-grid\"></div>');
      this.exifselected = $('<div class=\"chromatic-zoom-exif-selected\"></div>');
      this.exifnotselected = $('<div class=\"chromatic-zoom-exif-not-selected\"></div>');
      this.leftPanel = $('<div class=\"chromatic-left-panel chromatic-panel\"></div>');
      this.rightPanel = $('<div class=\"chromatic-right-panel chromatic-panel\"></div>');
      this.photoInfo = $('<div class="chromatic-zoom-photo-information"></div>');
      this.exifInfo = $('<div class=\"chromatic-zoom-exif-information\"></div>');
      this.navigation = $('<div class=\"chromatic-navigation\"></div>');
      this.clearfloat = '<div style="clear:both"></div>';
      this.navigation.append(this.leftarrow, this.grid, this.rightarrow, this.exifnotselected, this.exifselected, this.clearfloat);
      this.infoContainer = $('<div class=\"project-information\"></div>');
      this.photographerImageContainer = $('<img src=' + photos[0].photographerUrl + '></img>');
      this.pictitle = $('<span class=\"project-image-title\"></span>');
      this.copyright = $('<span class=\"project-image-copyright\"><i style=\"padding-right:3px;\" class=\"glyphicon glyphicon-copyright-mark\"></i></span>');
      this.link = $('<div class=\"links\"><a href="#/about">' + options.about + '</a> | <a href="#/contact">' + options.contact + '</a></div>');
      this.picdesc = $('<span class=\"project-image-desc\"></span>');
      this.picdescoriginal = $('<span class=\"project-image-desc-original\"></span>');
      this.infoContainer.append(this.photographerImageContainer, this.pictitle, this.picdesc, this.picdescoriginal, this.copyright, this.link);
      this.photodetailContainer = $('<div class=\"chromatic-photo-detail\"></div>');
      this.photoInfo = $('<div class=\"chromatic-zoom-photo-information\"></div>');
      this.photoInfo.append(this.infoContainer, this.navigation);
      this.exifContainer = $('<div class=\"project-exif-information\"></div>');
      this.exifComponent = $('<div class="exif-component"></div>');
      this.exifCamera = $('<div class="exif-individual-component">' + '<div class="exif-component-image">' + '<img src="../content/images/exif/camera.png">' + '</div>' + '<div class="info">' + '<span>CAMERA</span>' + '<p class="camera-info"></p>' + '</div>' + '</div>');
      this.exifLense = $('<div class="exif-individual-component">' + '<div class="exif-component-image">' + '<img src="../content/images/exif/lens.png">' + '</div>' + '<div class="info">' + '<span>LENS</span>' + '<p class="lense-info"></p>' + '</div>' + '</div>');
      this.exifShutterSpeed = $('<div class="exif-individual-component">' + '<div class="exif-component-image">' + '<img src="../content/images/exif/shutterspeed.png">' + '</div>' + '<div class="info">' + '<span>SHUTTER SPEED</span>' + '<p class="shutter-speed"></p>' + '</div>' + '</div>');
      this.exifFlash = $('<div class="exif-individual-component">' + '<div class="exif-component-image">' + '<img src="../content/images/exif/flash.png">' + '</div>' + '<div class="info">' + '<span>FLASH</span>' + '<p class="flash"></p>' + '</div>' + '</div>');
      this.exifFPoint = $('<div class="exif-individual-component">' + '<div class="exif-component-image">' + '<img src="../content/images/exif/f-stop.png">' + '</div>' + '<div class="info">' + '<span>F-POINT</span>' + '<p class="focalpoint"></p>' + '</div>' + '</div>');
      this.exifFocalLength = $('<div class="exif-individual-component">' + '<div class="exif-component-image">' + '<img src="../content/images/exif/focal-length.png">' + '</div>' + '<div class="info">' + '<span>FOCAL LENGTH</span>' + '<p class="focallength"></p>' + '</div>' + '</div>');
      this.exifISO = $('<div class="exif-individual-component">' + '<div class="exif-component-image">' + '<img src="../content/images/exif/i-s-o.png">' + '</div>' + '<div class="info">' + '<span>ISO</span>' + '<p class="iso"></p>' + '</div>' + '</div>');
      this.exifSoftware = $('<div class="exif-individual-component">' + '<div class="exif-component-image">' + '<img src="../content/images/exif/software.png">' + '</div>' + '<div class="info">' + '<span>SOFTWARE</span>' + '<p class="software"></p>' + '</div>' + '</div>');
      this.exifComponent.append(this.exifCamera, this.exifShutterSpeed, this.exifFPoint, this.exifISO, this.exifLense, this.exifFlash, this.exifFocalLength, this.exifSoftware, this.clearfloat);
      this.exifContainer.append(this.exifComponent, this.clearfloat);
      this.exifInfo.append(this.exifContainer);
      this.el.on('contextmenu', this.rightclick);
      this.photos = photos;
      this.interval = null;
      this.mousemoveTimeout = null;
      this.el.hide().on('click', '.closeZoom', this.close).on('click', '.chromatic-zoom-arrow-left', this.showPrevious).on('click', '.chromatic-zoom-arrow-right', this.showNext).on('click', '.chromatic-left-panel', this.showPrevious).on('click', '.chromatic-right-panel', this.showNext);
      this.el.on('mousemove', this.mousemovement);
      this.el.append(this.leftPanel, this.rightPanel);
      this._debouncedLayout = _.debounce(((function(_this) {
        return function() {
          return _this.layout();
        };
      })(this)), 100);
      this.currentSelectedView = null;
      if (el !== null) {
        this.el.on('click', '.chromatic-zoom-photo-grid', this.portfolio);
        $(el).append(this.el);
        this.exifnotselected.hide();
        this.exifselected.hide();
        this.photodetailContainer.append(this.photoInfo);
        this.el.append(this.photodetailContainer);
        this.show(photos[0]);
      } else {
        this.el.on('click', '.chromatic-zoom-photo-grid', this.close);
        $('.content-container').append(this.el);
        this.photodetailContainer.append(this.photoInfo, this.exifInfo);
        this.el.append(this.photodetailContainer);
        this.initExif();
      }
      $('.links a').on('click', this.close);
      this.attachhideInformationEvent();
    }

    ZoomView.prototype.initExif = function() {
      $('.chromatic-zoom-exif-not-selected').hide();
      $('.chromatic-zoom-exif-selected').hide();
      this.exifContainer.hide();
      this.exifShown = 'undefined';
      $('.chromatic-zoom-exif-not-selected').on('click', this.showExif);
      return $('.chromatic-zoom-exif-selected').on('click', this.hideExif);
    };

    ZoomView.prototype.showExif = function() {
      $('.chromatic-zoom-exif-not-selected').hide();
      $('.chromatic-zoom-exif-selected').show();
      this.exifShown = true;
      this.setheight();
      return this.exifContainer.show();
    };

    ZoomView.prototype.hideExif = function() {
      $('.chromatic-zoom-exif-not-selected').show();
      $('.chromatic-zoom-exif-selected').hide();
      this.exifShown = false;
      return this.exifContainer.hide();
    };

    ZoomView.prototype.noExif = function() {
      $('.chromatic-zoom-exif-not-selected').hide();
      $('.chromatic-zoom-exif-selected').hide();
      return this.exifContainer.hide();
    };

    ZoomView.prototype.startTimer = function() {
      var self;
      self = this;
      return this.interval = setInterval(function() {
        return self.showNext();
      }, 5000);
    };

    ZoomView.prototype.hideextrainfo = function() {
      return this.photodetailContainer.fadeOut(500);
    };

    ZoomView.prototype.hidefirstTime = function() {
      var self;
      self = this;
      return this.mousemoveTimeout = setTimeout(self.hideextrainfo, 3000);
    };

    ZoomView.prototype.attachhideInformationEvent = function() {
      $('.chromatic-zoom-photo-information').on('mouseenter', this.detachTimer);
      $('.project-exif-information').on('mouseenter', this.detachTimer);
      $('.chromatic-zoom-photo-information').on('mouseleave', this.attachTimer);
      return $('.project-exif-information').on('mouseleave', this.attachTimer);
    };

    ZoomView.prototype.attachTimer = function() {
      this.el.on('mousemove', this.mousemovement);
      if (this.mousemoveTimeout !== null) {
        clearTimeout(this.mousemoveTimeout);
        this.mousemoveTimeout = null;
        return this.hidefirstTime();
      } else if (this.mousemoveTimeout === null) {
        return this.hidefirstTime();
      }
    };

    ZoomView.prototype.detachTimer = function() {
      this.el.off('mousemove');
      this.photodetailContainer.fadeIn(100);
      if (this.mousemoveTimeout !== null) {
        clearTimeout(this.mousemoveTimeout);
        return this.mousemoveTimeout = null;
      }
    };

    ZoomView.prototype.mousemovement = function(event) {
      if (this.lastX !== event.clientX || this.lastY !== event.clientY) {
        this.lastX = event.clientX;
        this.lastY = event.clientY;
        this.photodetailContainer.fadeIn(100);
        if (this.mousemoveTimeout !== null) {
          clearTimeout(this.mousemoveTimeout);
          this.mousemoveTimeout = null;
          return this.hidefirstTime();
        }
      }
    };

    ZoomView.prototype.portfolio = function() {
      this.close();
      if (this.interval) {
        clearInterval(this.interval);
      }
      return window.location.hash = 'portfolio';
    };

    ZoomView.prototype.close = function() {
      if (this.mousemoveTimeout) {
        clearTimeout(this.mousemoveTimeout);
      }
      if (this.interval) {
        clearInterval(this.interval);
      }
      key.unbind('esc');
      key.unbind('enter');
      key.unbind('up');
      key.unbind('left');
      key.unbind('backspace');
      key.unbind('j');
      key.unbind('right');
      key.unbind('k');
      key.unbind('m');
      $(window).off('resize orientationchange', this._debouncedLayout);
      this.el.fadeOut(500, (function(_this) {
        return function() {
          $(document.body).css('paddingRight', '');
          $(document.body).css('overflowY', 'auto');
          _this.previous_zoom_photo_view.remove();
          _this.current_zoom_photo_view.remove();
          _this.next_zoom_photo_view.remove();
          _this.previous_zoom_photo_view = null;
          _this.current_zoom_photo_view = null;
          return _this.next_zoom_photo_view = null;
        };
      })(this));
      return true;
    };

    ZoomView.prototype.show = function(photo) {
      var next, previous;
      $(document.body).css('overflowY', 'hidden');
      if (this.container !== null) {
        this.startTimer();
        key('esc, enter, up', this.portfolio);
      } else {
        $(document.body).css('paddingRight', _scrollbar_width());
        key('esc, enter, up, backspace', this.close);
      }
      key('left, k', _.debounce(this.showPrevious, 100, true));
      key('right, j', _.debounce(this.showNext, 100, true));
      key('m', this.showUrl);
      $(window).on('resize orientationchange', this._debouncedLayout);
      this.el.fadeIn(500);
      this.hidefirstTime();
      if (this.previous_zoom_photo_view) {
        this.previous_zoom_photo_view.remove();
      }
      if (this.current_zoom_photo_view) {
        this.current_zoom_photo_view.remove();
      }
      if (this.next_zoom_photo_view) {
        this.next_zoom_photo_view.remove();
      }
      previous = this.photos[this.photos.indexOf(photo) - 1] || this.photos[this.photos.length - 1];
      this.current = photo;
      next = this.photos[this.photos.indexOf(photo) + 1] || this.photos[0];
      this.previous_zoom_photo_view = new Chromatic.ZoomPhotoView(this, previous);
      this.current_zoom_photo_view = new Chromatic.ZoomPhotoView(this, this.current);
      this.next_zoom_photo_view = new Chromatic.ZoomPhotoView(this, next);
      this.bindData(this.current, this.current_zoom_photo_view);
      this.layout();
      return this.el.show();
    };

    ZoomView.prototype.showNext = function(e) {
      var next;
      if (e) {
        e.preventDefault();
        e.stopPropagation();
        if (this.interval) {
          clearInterval(this.interval);
        }
      }
      if (this.previous_zoom_photo_view != null) {
        this.previous_zoom_photo_view.remove();
      }
      this.previous_zoom_photo_view = this.current_zoom_photo_view;
      this.current_zoom_photo_view = this.next_zoom_photo_view;
      this.current = this.photos[this.photos.indexOf(this.current) + 1] || this.photos[0];
      next = this.photos[this.photos.indexOf(this.current) + 1] || this.photos[0];
      this.next_zoom_photo_view = new Chromatic.ZoomPhotoView(this, next);
      if (this.current_zoom_photo_view != null) {
        this.current_zoom_photo_view.layout('current', 0, true, this.container);
      }
      if (this.previous_zoom_photo_view != null) {
        this.previous_zoom_photo_view.layout('previous', 0, true, this.container);
      }
      this.next_zoom_photo_view.layout('next', 0, false, this.container);
      return this.bindData(this.current, this.current_zoom_photo_view);
    };

    ZoomView.prototype.showPrevious = function(e) {
      var previous;
      if (e) {
        e.preventDefault();
        e.stopPropagation();
        if (this.interval) {
          clearInterval(this.interval);
        }
      }
      this.next_zoom_photo_view.remove();
      this.next_zoom_photo_view = this.current_zoom_photo_view;
      this.current_zoom_photo_view = this.previous_zoom_photo_view;
      this.current = this.photos[this.photos.indexOf(this.current) - 1] || this.photos[this.photos.length - 1];
      previous = this.photos[this.photos.indexOf(this.current) - 1] || this.photos[this.photos.length - 1];
      this.previous_zoom_photo_view = new Chromatic.ZoomPhotoView(this, previous);
      this.next_zoom_photo_view.layout('next', 0, true, this.container);
      this.current_zoom_photo_view.layout('current', 0, true, this.container);
      this.previous_zoom_photo_view.layout('previous', 0, false, this.container);
      return this.bindData(this.current, this.current_zoom_photo_view);
    };

    ZoomView.prototype.bindData = function(current, currentview) {
      this.currentSelectedView = currentview;
      $('.project-image-title').text(current.title.toUpperCase());
      $('.project-image-copyright').html('&copy;&nbsp' + current.photographerName.toUpperCase());
      $('.project-image-desc').text(current.desc);
      $('.project-image-desc-original').text(current.originaldesc);
      this.attachclick();
      if (this.container === null) {
        return currentview.getExifInfo(this.bindExif);
      } else {

      }
    };

    ZoomView.prototype.bindExif = function(photoid, data) {
      if (data && data.camera) {
        if (this.exifShown === 'undefined') {
          this.exifShown = false;
          $('.chromatic-zoom-exif-not-selected').show();
        }
        $('.camera-info').text(this.setExifData(data.camera));
        $('.camera-info').prop('title', data.camera);
        $('.lense-info').text(this.setExifData(data.lensModel));
        $('.lense-info').prop('title', data.lensModel);
        $('.shutter-speed').text(this.setExifData(data.exposureTime));
        $('.shutter-speed').prop('title', data.exposureTime);
        $('.flash').text(this.setExifData(data.flash));
        $('.flash').prop('title', data.flash);
        $('.focalpoint').text('/' + this.setExifData(data.fNumber));
        $('.focalpoint').prop('title', '/' + data.fNumber);
        $('.focallength').text(this.setExifData(data.focalLength));
        $('.focallength').prop('title', data.focalLength);
        $('.iso').text(this.setExifData(data.iso));
        $('.iso').prop('title', data.iso);
        $('.software').text(this.setExifData(data.software));
        $('.software').prop('title', data.software);
        if (this.exifShown === true) {
          $('.chromatic-zoom-exif-selected').show();
          return this.exifContainer.show();
        } else {
          $('.chromatic-zoom-exif-not-selected').show();
          return this.exifContainer.hide();
        }
      } else {
        this.exifContainer.hide();
        $('.chromatic-zoom-exif-not-selected').hide();
        return $('.chromatic-zoom-exif-selected').hide();
      }
    };

    ZoomView.prototype.setExifData = function(data) {
      if (data === null) {
        return '-';
      }
      if (typeof data !== 'undefind' && data.toString().length === 0) {
        return '-';
      } else if (typeof data !== 'undefined' && data.toString().length < 15) {
        return data;
      } else if (typeof data !== 'undefined') {
        return data.toString().substring(0, 12) + '...';
      } else {
        return '-';
      }
    };

    ZoomView.prototype.setheight = function() {
      return this.photodetailContainer.css({
        height: this.exifComponent.height() < (this.infoContainer.height() + 28) ? this.infoContainer.height() + 80 : this.exifComponent.height()
      });
    };

    ZoomView.prototype.layout = function(offset, animated) {
      if (offset == null) {
        offset = 0;
      }
      if (this.current_zoom_photo_view != null) {
        this.current_zoom_photo_view.layout('current', offset, animated, this.container);
      }
      if (this.previous_zoom_photo_view != null) {
        this.previous_zoom_photo_view.layout('previous', offset, animated, this.container);
      }
      if (this.next_zoom_photo_view != null) {
        this.next_zoom_photo_view.layout('next', offset, animated, this.container);
      }
      return this.setheight();
    };

    ZoomView.prototype.attachclick = function() {
      $('.project-image-desc').click(function() {
        $('.project-image-desc').css('display', 'none');
        $('.project-image-desc-original').css('display', 'block');
        return true;
      });
      return $('.project-image-desc-original').click(function() {
        $('.project-image-desc').css('display', 'block');
        $('.project-image-desc-original').css('display', 'none');
        return true;
      });
    };

    ZoomView.prototype.rightclick = function() {
      return false;
    };

    ZoomView.prototype.showUrl = function() {
      return alert(this.currentSelectedView.photo.big);
    };

    return ZoomView;

  })();

}).call(this);

(function() {
  var _is_css_blur_supported,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  this.Chromatic = this.Chromatic || {};

  _is_css_blur_supported = (function() {
    var _supported;
    _supported = 'dontknow';
    return function() {
      var el;
      if (_supported !== 'dontknow') {
        return _supported;
      }
      el = $('<div/>');
      $(document.body).append(el);
      el[0].style.webkitFilter = "grayscale(1)";
      _supported = window.getComputedStyle(el[0]).webkitFilter === "grayscale(1)";
      el.remove();
      return _supported;
    };
  })();

  Chromatic.ZoomPhotoView = (function() {
    function ZoomPhotoView(parent, photo, options) {
      this.getExifInfo = bind(this.getExifInfo, this);
      this.layout = bind(this.layout, this);
      this.render = bind(this.render, this);
      this.remove = bind(this.remove, this);
      this.photo = photo;
      this.el = $('<div/>');
      this.render();
      parent.el.append(this.el);
      this.exif = {};
    }

    ZoomPhotoView.prototype.remove = function(photo) {
      return this.el.remove();
    };

    ZoomPhotoView.prototype.render = function() {
      var big_img;
      this.photo_el = $('<div class="chromatic-zoom-photo"></div>');
      this.grain_el = $('<div class="chromatic-zoom-grain"></div>');
      this.background_el = $('<div class="chromatic-zoom-background"></div>');
      if (this.photo.big) {
        big_img = new Image();
        big_img.onload = (function(_this) {
          return function() {
            return _this.photo_el.css('backgroundImage', "url(" + _this.photo.big + ")");
          };
        })(this);
        big_img.src = this.photo.big;
      }
      this.photo_el.css('backgroundImage', "url(" + this.photo.small + ")");
      this.backgroundenable = this.photo.enable;
      if (this.photo.blur && this.backgroundenable) {
        this.background_el.css('backgroundImage', "url(" + this.photo.blur + ")");
      } else if (_is_css_blur_supported() && this.backgroundenable) {
        this.background_el.addClass('chromatic-zoom-background-blur').css('backgroundImage', "url(" + this.photo.small + ")");
      } else {
        this.background_el.addClass('chromatic-zoom-background-blur').css('backgroundImage', 'none');
      }
      this.el.append(this.photoInfo, this.photo_el, this.grain_el, this.background_el);
      return this;
    };

    ZoomPhotoView.prototype.layout = function(pos, offset, animated, view) {
      var container, height, left, opacity, width;
      if (offset == null) {
        offset = 0;
      }
      container = $(window);
      if ((this.photo.bigHeight / container.height()) < 0.8 && (this.photo.bigWidth / container.width()) < 0.8) {
        width = this.photo.bigWidth;
        height = this.photo.bigHeight;
      } else if (view !== null) {
        if (this.photo.aspect_ratio > 1) {
          width = container.width();
          height = container.width() / this.photo.aspect_ratio;
        } else if (container.width() / container.height() > this.photo.aspect_ratio) {
          height = container.height();
          width = container.height() * this.photo.aspect_ratio;
        } else {
          height = container.width() / this.photo.aspect_ratio;
          width = container.width();
        }
      } else {
        if (this.photo.bigHeight / container.height() > this.photo.bigWidth / container.width()) {
          height = container.height();
          width = container.height() * this.photo.aspect_ratio;
        } else {
          width = container.width();
          height = container.width() / this.photo.aspect_ratio;
        }
      }
      this.photo_el.css({
        height: height,
        width: width,
        top: (container.height() - height) / 2
      });
      left = (function() {
        switch (pos) {
          case 'previous':
            return -width - 20 + offset;
          case 'current':
            this.temp = (container.width() - width) / 2 + offset;
            this.photo_el.css('left', this.temp);
            return this.temp;
          case 'next':
            return container.width() + 20 + offset;
        }
      }).call(this);
      opacity = (function() {
        switch (pos) {
          case 'current':
            return 1 - Math.abs(offset) / container.width() * 2;
          case 'previous':
            return 0 + offset / container.width() * 2;
          case 'next':
            return 0 - offset / container.width() * 2;
        }
      })();
      if (animated) {
        this.photo_el.stop().animate({
          opacity: opacity
        }, 1000, function() {
          return $(this).css('left', left);
        });
        this.grain_el.stop().animate({
          opacity: opacity
        }, 1000);
        return this.background_el.stop().animate({
          opacity: opacity
        }, 1000);
      } else {
        this.photo_el.css('opacity', opacity);
        this.grain_el.css('opacity', opacity);
        return this.background_el.css('opacity', opacity);
      }
    };

    ZoomPhotoView.prototype.getExifInfo = function(callback) {
      this.url = 'http://siftr.co/api/' + this.photo.photographerid + '/photos/' + this.photo.id + '/exif/';
      return $.ajax(this.url, {
        type: 'GET',
        success: (function(_this) {
          return function(data, textStatus, jqXHR) {
            _this.exif = data;
            return callback(_this.photo.id, data);
          };
        })(this),
        error: (function(_this) {
          return function(jqXHR, ajaxOptions, errorThrown) {
            if (jqXHR.status === 404) {
              _this.exif = null;
              return callback(_this.photo.id, null);
            }
          };
        })(this)
      });
    };

    return ZoomPhotoView;

  })();

}).call(this);

(function() {
  $.fn.extend({
    chromatic: function(photos, options) {
      new Chromatic.GalleryView(this, photos, options);
      return this;
    },
    chromaticZoom: function(photos, options) {
      new Chromatic.ZoomView(this, photos, options);
      return this;
    }
  });

}).call(this);
