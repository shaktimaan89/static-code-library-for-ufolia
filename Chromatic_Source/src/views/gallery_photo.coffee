@Chromatic = @Chromatic or {}

class Chromatic.GalleryPhotoView
  constructor: (parent, photo, options) ->
    @parent = parent
    @photo  = photo
    @theme = options.theme
    @isCard = options.isCard
    @title=photo.title
    @el     = $('<div class="chromatic-gallery-photo"></div>')
    if @isCard
      @el.addClass('chromatic-card');
    @elimage= $('<img id="'+@photo.id+'image" class="chromatic-gallery-photo-image"></img>')
    @elcanvas=$('<canvas class="chromatic-gallery-photo-image" id="'+@photo.id+'"></canvas>');
    @el.on 'contextmenu', @rightclick
    parent.el.append(@el)
    @el.on  'click', @zoom

  load: (callback) =>
    return  if @loaded
      callback()
    @colorList=['#f2f4ff','#f2fffd','#f3f9ff','#f2fcff','#f2fff8','#f0f6fc','#f6fff2']
    @colorListTheme=['#382d2d','#252f31','#343833','#313236','#373237']
    if @theme
      @index=@photo.key%5
      @color=@colorListTheme[@index]
    else
      @index=@photo.key%7
      @color=@colorList[@index]
    @el.css('background-color',@color)
    image = new Image()
    image.crossOrigin='Anonymous'
    image.onload = =>
      @photo.aspect_ratio = image.width/image.height
      @photo.width=image.width
      @photo.height=image.height
      callback()
      @elimage.attr('src',@photo.low)
      @el.append(@elimage)
      @elimage.addClass('loaded-canvas')
      @el.append(@elcanvas)
      stackBlurImage(image,@photo.id,30, false );
      @loaded = true
    image.onerror = =>
      callback()
    if typeof @photo.low != 'undefined' && @photo.source=='px'
      image.src = 'https://farm6.staticflickr.com/5060/5444250007_1392c5bf6d_s.jpg'
    else if typeof @photo.low != 'undefined'
      image.src = @photo.low
    else
      image.src = @photo.small

  loadhighres: (callback)=>
    return if @loaded && !@photo.small
      callback('0',@photo.photographerid,@photo.id,@photo.alias,@photo.source,@photo.low)
    image = new Image()
    image.onload = =>
      @photo.aspect_ratio = image.width/image.height
      @photo.width=image.width
      @photo.height=image.height
      callback('1',@photo.photographerid,@photo.id,@photo.alias,@photo.source,@photo.low)
      @elimage.attr('src',@photo.small)
      @elcanvas.addClass('loaded-canvas')
      @elimage.addClass('loaded-image')
      @loaded = true
    image.onerror= =>
      callback('2',@photo.photographerid,@photo.id,@photo.alias,@photo.source,@photo.low) if callback
    image.src = @photo.small


  unload: =>
    @el.css('backgroundImage', "")
    @el.remove()
    @loaded = false

  zoom: =>
    @parent.zoom(@photo)
    @parent.el.click()

  resize: (width, height) ->
    @el.css
      width: width - parseInt(@el.css('marginLeft'))-parseInt(@el.css('marginRight'))
      height: height - parseInt(@el.css('marginTop'))-parseInt(@el.css('marginBottom')) 
    @top = @el.position().top
    @bottom = @top + @el.height()

  setposition: (left,top)->
    @el.css
      top:top
      left:left

  rightclick: =>
    return false  