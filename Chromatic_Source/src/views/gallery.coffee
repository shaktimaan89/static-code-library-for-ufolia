@Chromatic = @Chromatic or {}

# Based on Óscar López implementation in Python (http://stackoverflow.com/a/7942946)
_linear_partition = (->
  _cache = {}

  return (seq, k) ->
    key = seq.join() + k
    return _cache[key] if _cache[key]

    n = seq.length

    return [] if k <= 0
    return seq.map((x) -> [x]) if k > n

    table = (0 for x in [0...k] for y in [0...n])
    solution = (0 for x in [0...k-1] for y in [0...n-1])
    table[i][0] = seq[i] + (if i then table[i-1][0] else 0) for i in [0...n]
    table[0][j] = seq[0] for j in [0...k]
    for i in [1...n]
      for j in [1...k]
        m = _.min(([_.max([table[x][j-1], table[i][0]-table[x][0]]), x] for x in [0...i]), (o) -> o[0])
        table[i][j] = m[0]
        solution[i-1][j-1] = m[1]

    n = n-1
    k = k-2
    ans = []
    while k >= 0
      ans = [seq[i] for i in [(solution[n-1][k]+1)...n+1]].concat ans
      n = solution[n-1][k]
      k = k-1

    _cache[key] = [seq[i] for i in [0...n+1]].concat ans
)()

_scrollbar_width = (->
  _cache = null
  return ->
    return _cache if _cache
    div = $("<div style=\"width:50px;height:50px;overflow:hidden;position:absolute;top:-200px;left:-200px;\"><div style=\"height:100px;\"></div></div>")
    $(document.body).append div
    w1 = $("div", div).innerWidth()
    div.css "overflow-y", "auto"
    w2 = $("div", div).innerWidth()
    $(div).remove()
    _cache = w1 - w2
)()

class Chromatic.GalleryView
  constructor: (el, photos, options) ->
    @el = $(el).addClass('chromatic-gallery')
    @photos       = _.map photos.photolist, (p) -> if _.isObject(p) then p else {small: p}
    @option=
      theme:photos.theme
      isCard:photos.isCard
      about:photos.about
      contact:photos.contact
    window.flickrurlcollection={}
    @zoom_view    = new Chromatic.ZoomView(null,@photos, @option)
    @photo_views  = _.map @photos, (photo) => new Chromatic.GalleryPhotoView(this, photo, @option)
    @ideal_height = parseInt(@el.children().first().css('height'))
    $(window).on 'resize', _.debounce(@layout, 100)
    @layout()
   
  loadhighresphoto: =>
    self=this
    _.each @photo_views, (p) -> p.loadhighres(self.checkloading)

  lazyLoad: =>
    @count=@photo_views.length
    threshold = 1000
    viewport_top = @el.scrollTop() - threshold
    viewport_bottom = (@el.height() || $(window).height()) + @el.scrollTop() + threshold
    _.each @photo_views, (photo_view) =>
        photo_view.load(@checkloadinglow)

  checkoverflow: =>
    try
      $('#left').css
        maxWidth:$('.navigation')[0].getBoundingClientRect().width
    catch error

  checkloadinglow:=>
    @count=@count-1
    if @count==0
      @loadhighresphoto()

  checkloading: (loaded,photographerid,photoid,alias,source,photourl)=>
    if loaded=='1'
      @checkdeletephoto(photographerid,photoid,alias,source,photourl)
    else if loaded=='2'
      @senddelete(alias,photographerid,photoid)

  checkdeletephoto: (photographerid,photoid,alias,source,photourl) =>
    if source=='Flickr'
      jsonp=@jsonp_custom(@successcallback,photourl,alias,photographerid,photoid)
      @url='http://www.flickr.com/services/oembed/?format=json&jsoncallback='+jsonp+'&url='+encodeURIComponent(photourl)
      $.ajax @url,
        type:'GET',
        dataType:'JSONP',
        jsonpCallback:'callback',
        error:(jqXHR,textStatus,errorThrown) =>
          if jqXHR.status==404 || jqXHR.status==401
            @senddelete(alias,photographerid,photoid)

  jsonp_custom:(callback,photourl,alias,photographerid,photoid)=>
    callback_name='jsonp_callback_'+Math.floor(Math.random()*100000)
    window[callback_name]=(data)=>
      callback(data,photourl,alias,photographerid,photoid)
      delete window[callback_name]
    return callback_name

  successcallback:(data,photourl,alias,photographerid,photoid) =>
    if((photourl.length>0 && data && !data.url)||(photourl.substring(0,photourl.length-5)!=data.url.substring(0,data.url.length-5)))
      @senddelete(alias,photographerid,photoid)
    return

  senddelete:(alias,photographerid,photoid)=>
    deleteurl='/api/'+photographerid+'/photos/'+photoid+'/'
    $.ajax deleteurl,
        type:'DELETE'

  layout: =>
    $(document.body).css('overflowY', 'scroll')
    viewport_width = @el[0].getBoundingClientRect().width - parseInt(@el.css('paddingLeft')) - parseInt(@el.css('paddingRight'))
    viewport_width=viewport_width-_scrollbar_width() if @el[0].offsetWidth > @el[0].scrollWidth
    $(document.body).css('overflowY', 'auto')
    if @option.isCard
      ideal_width=300
      column=Math.round(viewport_width/ideal_width)
      widthapprox=Math.floor(viewport_width/column)
      if column < 1
        _.each @photos, (photo, i) => @photo_views[i].resize parseInt(ideal_height * photo.aspect_ratio), ideal_height
      else
        weights = _.map @photos, (p) -> parseInt(100/p.aspect_ratio)
        partition = _linear_partition(weights, column)
        index = 0
        left=0
        maxHeight=0
        _.each partition, (column) =>
          row_buffer = []
          top=0
          _.each column, (p, i) => row_buffer.push(@photos[index+i])
          _.each row_buffer, (p, i) =>
              width  = widthapprox
              height = width/@photo_views[index+i].photo.aspect_ratio
              @photo_views[index+i].resize width, height
              @photo_views[index+i].setposition left, top
              top=top+height
          if top>=maxHeight
            maxHeight=top
          index += column.length
          left=left+widthapprox    
        @el.css
          height:maxHeight
    else
      ideal_height   = @ideal_height || parseInt((@el.height() || $(window).width()) / 2)
      summed_width   = _.reduce @photos, ((sum, p) -> sum += p.aspect_ratio * ideal_height), 0
      rows           = Math.round(summed_width / viewport_width)
      if rows < 1
        _.each @photos, (photo, i) => @photo_views[i].resize parseInt(ideal_height * photo.aspect_ratio), ideal_height
      else
        weights = _.map @photos, (p) -> parseInt(p.aspect_ratio * 100)
        partition = _linear_partition(weights, rows)
        index = 0
        _.each partition, (row) =>
          row_buffer = []
          _.each row, (p, i) => row_buffer.push(@photos[index+i])
          summed_ars = _.reduce row_buffer, ((sum, p) -> sum += p.aspect_ratio), 0
          summed_width = 0
          _.each row_buffer, (p, i) =>
            width  = if i == row_buffer.length-1 then viewport_width - summed_width else parseInt(viewport_width / summed_ars * p.aspect_ratio)
            height = parseInt(viewport_width / summed_ars)
            @photo_views[index+i].resize width, height
            summed_width += width
          index += row.length
    @lazyLoad()
    @checkoverflow()

  zoom: (photo) => @zoom_view.show(photo)