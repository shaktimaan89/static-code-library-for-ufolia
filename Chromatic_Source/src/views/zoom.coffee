@Chromatic = @Chromatic or {}

# Extend jquery with easeOut transition (loading jquery UI would be overkill)
$.extend $.easing,
  easeOutCirc: (x, t, b, c, d) ->
    return c * Math.sqrt(1 - (t=t/d-1)*t) + b

_scrollbar_width = (->
  _cache = null
  return ->
    return _cache if _cache
    div = $("<div style=\"width:50px;height:50px;overflow:hidden;position:absolute;top:-200px;left:-200px;\"><div style=\"height:100px;\"></div></div>")
    $(document.body).append div
    w1 = $("div", div).innerWidth()
    div.css "overflow-y", "auto"
    w2 = $("div", div).innerWidth()
    $(div).remove()
    _cache = w1 - w2
)()

class Chromatic.ZoomView
  constructor: (el, photos, options) ->
    @container=el
    @el = $('<div class="chromatic-zoom"/>')
    @leftarrow = $('<div class=\"chromatic-zoom-arrow-left\"></div>')
    @rightarrow = $('<div class=\"chromatic-zoom-arrow-right\"></div>')
    @grid = $('<div class=\"chromatic-zoom-photo-grid\"></div>')
    @exifselected = $('<div class=\"chromatic-zoom-exif-selected\"></div>')
    @exifnotselected = $('<div class=\"chromatic-zoom-exif-not-selected\"></div>')
    @leftPanel = $('<div class=\"chromatic-left-panel chromatic-panel\"></div>')
    @rightPanel = $('<div class=\"chromatic-right-panel chromatic-panel\"></div>')
    @photoInfo = $('<div class="chromatic-zoom-photo-information"></div>')
    @exifInfo = $('<div class=\"chromatic-zoom-exif-information\"></div>')
    @navigation=$('<div class=\"chromatic-navigation\"></div>')
    @clearfloat=('<div style="clear:both"></div>')
    @navigation.append(@leftarrow,@grid,@rightarrow,@exifnotselected,@exifselected,@clearfloat)
    @infoContainer=$('<div class=\"project-information\"></div>')
    @photographerImageContainer =$('<img src='+photos[0].photographerUrl+'></img>')
    @pictitle = $('<span class=\"project-image-title\"></span>')
    @copyright= $('<span class=\"project-image-copyright\"><i style=\"padding-right:3px;\" class=\"glyphicon glyphicon-copyright-mark\"></i></span>')
    @link = $('<div class=\"links\"><a href="#/about">'+options.about+'</a> | <a href="#/contact">'+options.contact+'</a></div>')
    @picdesc = $('<span class=\"project-image-desc\"></span>')
    @picdescoriginal = $('<span class=\"project-image-desc-original\"></span>')
    @infoContainer.append(@photographerImageContainer,@pictitle,@picdesc,@picdescoriginal,@copyright,@link)
    @photodetailContainer=$('<div class=\"chromatic-photo-detail\"></div>')
    @photoInfo = $('<div class=\"chromatic-zoom-photo-information\"></div>')
    @photoInfo.append(@infoContainer,@navigation)
    @exifContainer=$('<div class=\"project-exif-information\"></div>')
    @exifComponent=$('<div class="exif-component"></div>')
    @exifCamera=$('<div class="exif-individual-component">'+
      '<div class="exif-component-image">'+
        '<img src="../content/images/exif/camera.png">'+
      '</div>'+
      '<div class="info">'+
        '<span>CAMERA</span>'+
        '<p class="camera-info"></p>'+
      '</div>'+
    '</div>')
    @exifLense=$('<div class="exif-individual-component">'+
      '<div class="exif-component-image">'+
        '<img src="../content/images/exif/lens.png">'+
      '</div>'+
      '<div class="info">'+
        '<span>LENS</span>'+
        '<p class="lense-info"></p>'+
      '</div>'+
    '</div>')
    @exifShutterSpeed=$('<div class="exif-individual-component">'+
      '<div class="exif-component-image">'+
        '<img src="../content/images/exif/shutterspeed.png">'+
      '</div>'+
      '<div class="info">'+
        '<span>SHUTTER SPEED</span>'+
        '<p class="shutter-speed"></p>'+
      '</div>'+
    '</div>')
    @exifFlash=$('<div class="exif-individual-component">'+
      '<div class="exif-component-image">'+
        '<img src="../content/images/exif/flash.png">'+
      '</div>'+
      '<div class="info">'+
        '<span>FLASH</span>'+
        '<p class="flash"></p>'+
      '</div>'+
    '</div>')
    @exifFPoint=$('<div class="exif-individual-component">'+
      '<div class="exif-component-image">'+
        '<img src="../content/images/exif/f-stop.png">'+
      '</div>'+
      '<div class="info">'+
        '<span>F-POINT</span>'+
        '<p class="focalpoint"></p>'+
      '</div>'+
    '</div>')
    @exifFocalLength=$('<div class="exif-individual-component">'+
      '<div class="exif-component-image">'+
        '<img src="../content/images/exif/focal-length.png">'+
      '</div>'+
      '<div class="info">'+
        '<span>FOCAL LENGTH</span>'+
        '<p class="focallength"></p>'+
      '</div>'+
    '</div>')
    @exifISO=$('<div class="exif-individual-component">'+
      '<div class="exif-component-image">'+
        '<img src="../content/images/exif/i-s-o.png">'+
      '</div>'+
      '<div class="info">'+
        '<span>ISO</span>'+
        '<p class="iso"></p>'+
      '</div>'+
    '</div>')
    @exifSoftware=$('<div class="exif-individual-component">'+
      '<div class="exif-component-image">'+
        '<img src="../content/images/exif/software.png">'+
      '</div>'+
      '<div class="info">'+
        '<span>SOFTWARE</span>'+
        '<p class="software"></p>'+
      '</div>'+
    '</div>')
    @exifComponent.append(@exifCamera,@exifShutterSpeed,@exifFPoint,@exifISO,@exifLense,@exifFlash,@exifFocalLength,@exifSoftware,@clearfloat)
    @exifContainer.append(@exifComponent,@clearfloat)
    @exifInfo.append(@exifContainer)
    @el.on 'contextmenu', @rightclick
    @photos = photos
    @interval=null
    @mousemoveTimeout=null
    #@_debouncedMouseMovement=_.debounce((=>@mousemovement()),200)
    @el.hide()
      .on('click','.closeZoom',@close)
      .on('click', '.chromatic-zoom-arrow-left', @showPrevious)
      .on('click', '.chromatic-zoom-arrow-right', @showNext)
      .on('click','.chromatic-left-panel',@showPrevious)
      .on('click','.chromatic-right-panel',@showNext)
    @el.on('mousemove',@mousemovement)
    @el.append(@leftPanel,@rightPanel)
    @_debouncedLayout = _.debounce((=> @layout()), 100)
    @currentSelectedView=null
    if el !=null
      @el.on('click','.chromatic-zoom-photo-grid', @portfolio)
      $(el).append(@el)
      @exifnotselected.hide()
      @exifselected.hide()
      @photodetailContainer.append(@photoInfo)
      @el.append(@photodetailContainer)
      @show(photos[0])
    else
      @el.on('click','.chromatic-zoom-photo-grid', @close)
      $('.content-container').append(@el)
      @photodetailContainer.append(@photoInfo,@exifInfo)
      @el.append(@photodetailContainer)
      @initExif()
    $('.links a').on('click',@close)
    @attachhideInformationEvent()
    
  initExif: =>
    $('.chromatic-zoom-exif-not-selected').hide()
    $('.chromatic-zoom-exif-selected').hide()
    @exifContainer.hide()
    @exifShown='undefined'
    $('.chromatic-zoom-exif-not-selected').on('click',@showExif)
    $('.chromatic-zoom-exif-selected').on('click',@hideExif)

  showExif: =>
    $('.chromatic-zoom-exif-not-selected').hide()
    $('.chromatic-zoom-exif-selected').show()
    @exifShown=true
    @setheight()
    @exifContainer.show()

  hideExif: =>
    $('.chromatic-zoom-exif-not-selected').show()
    $('.chromatic-zoom-exif-selected').hide()
    @exifShown=false
    @exifContainer.hide()

  noExif: =>
    $('.chromatic-zoom-exif-not-selected').hide()
    $('.chromatic-zoom-exif-selected').hide()
    @exifContainer.hide()
      
  startTimer: =>
    self=this
    @interval=setInterval ()-> 
        self.showNext()
      ,5000

  hideextrainfo: =>
      @photodetailContainer.fadeOut(500)
  
  hidefirstTime:=>
    self=this
    @mousemoveTimeout=setTimeout(self.hideextrainfo,3000)

  attachhideInformationEvent:=>
    $('.chromatic-zoom-photo-information').on('mouseenter',@detachTimer)
    $('.project-exif-information').on('mouseenter',@detachTimer)
    $('.chromatic-zoom-photo-information').on('mouseleave',@attachTimer)
    $('.project-exif-information').on('mouseleave',@attachTimer)

  attachTimer:=>
    @el.on('mousemove',@mousemovement)
    if @mousemoveTimeout!=null
      clearTimeout(@mousemoveTimeout)
      @mousemoveTimeout=null
      @hidefirstTime()
    else if @mousemoveTimeout==null
      @hidefirstTime()

  detachTimer:=>
    @el.off 'mousemove'
    @photodetailContainer.fadeIn(100)
    if @mousemoveTimeout!=null
      clearTimeout(@mousemoveTimeout)
      @mousemoveTimeout=null


  mousemovement: (event) =>
    if @lastX!=event.clientX || @lastY!=event.clientY
      @lastX=event.clientX
      @lastY=event.clientY
      @photodetailContainer.fadeIn(100)
      if @mousemoveTimeout!=null
        clearTimeout(@mousemoveTimeout)
        @mousemoveTimeout=null
        @hidefirstTime()
   

  portfolio: =>
    @close()
    clearInterval(@interval) if @interval
    window.location.hash = 'portfolio';

  close: =>
    clearTimeout(@mousemoveTimeout) if @mousemoveTimeout
    clearInterval(@interval) if @interval
    key.unbind 'esc'; key.unbind 'enter'; key.unbind 'up'; key.unbind 'left'; key.unbind 'backspace'; key.unbind 'j'; key.unbind 'right'; key.unbind 'k'; key.unbind 'm'; # doesnt support multiple keys
    $(window).off 'resize orientationchange', @_debouncedLayout
    @el.fadeOut 500, =>
      $(document.body).css('paddingRight','')
      $(document.body).css('overflowY', 'auto')
      @previous_zoom_photo_view.remove()
      @current_zoom_photo_view.remove()
      @next_zoom_photo_view.remove()
      @previous_zoom_photo_view = null
      @current_zoom_photo_view  = null
      @next_zoom_photo_view     = null
    return true

  show: (photo) =>
    $(document.body).css('overflowY', 'hidden') # prevent translucent scrollbars
    if @container!= null
      @startTimer()
      key 'esc, enter, up', @portfolio
    else
      $(document.body).css('paddingRight',_scrollbar_width())
      key 'esc, enter, up, backspace', @close
    key 'left, k',        _.debounce(@showPrevious, 100, true)
    key 'right, j',       _.debounce(@showNext, 100, true)
    key 'm', @showUrl
    $(window).on 'resize orientationchange', @_debouncedLayout
    @el.fadeIn(500)
    @hidefirstTime()
    @previous_zoom_photo_view.remove() if @previous_zoom_photo_view
    @current_zoom_photo_view.remove()  if @current_zoom_photo_view
    @next_zoom_photo_view.remove()     if @next_zoom_photo_view
    previous  = @photos[@photos.indexOf(photo) - 1] || @photos[@photos.length-1]
    @current  = photo
    next      = @photos[@photos.indexOf(photo) + 1] || @photos[0]
    @previous_zoom_photo_view = new Chromatic.ZoomPhotoView(this, previous)
    @current_zoom_photo_view  = new Chromatic.ZoomPhotoView(this, @current)
    @next_zoom_photo_view     = new Chromatic.ZoomPhotoView(this, next)
    #bind the data
    @bindData(@current,@current_zoom_photo_view)
    @layout()
    @el.show()

  showNext: (e) =>
    if e
      e.preventDefault()
      e.stopPropagation()
      clearInterval(@interval) if(@interval)
    @previous_zoom_photo_view.remove() if @previous_zoom_photo_view?
    #@previous_zoom_photo_view = null
    @previous_zoom_photo_view = @current_zoom_photo_view
    @current_zoom_photo_view  = @next_zoom_photo_view
    @current  = @photos[@photos.indexOf(@current) + 1] || @photos[0]
    next      = @photos[@photos.indexOf(@current) + 1] || @photos[0]
    @next_zoom_photo_view = new Chromatic.ZoomPhotoView(this, next)
    @current_zoom_photo_view.layout('current', 0, true,@container) if @current_zoom_photo_view?
    @previous_zoom_photo_view.layout('previous', 0, true,@container) if @previous_zoom_photo_view?
    @next_zoom_photo_view.layout('next', 0, false,@container)
    #bind the data
    @bindData(@current,@current_zoom_photo_view)

  showPrevious: (e) =>
    if e
      e.preventDefault()
      e.stopPropagation()
      clearInterval(@interval) if(@interval)
    @next_zoom_photo_view.remove()
    #@next_zoom_photo_view = null
    @next_zoom_photo_view = @current_zoom_photo_view
    @current_zoom_photo_view = @previous_zoom_photo_view
    @current  = @photos[@photos.indexOf(@current) - 1] || @photos[@photos.length-1]
    previous  = @photos[@photos.indexOf(@current) - 1] || @photos[@photos.length-1]
    @previous_zoom_photo_view = new Chromatic.ZoomPhotoView(this, previous)
    @next_zoom_photo_view.layout('next', 0, true,@container)
    @current_zoom_photo_view.layout('current', 0, true,@container)
    @previous_zoom_photo_view.layout('previous', 0, false,@container)
    #bind the data
    @bindData(@current,@current_zoom_photo_view)

  bindData: (current,currentview) =>
    @currentSelectedView=currentview
    $('.project-image-title').text(current.title.toUpperCase())
    $('.project-image-copyright').html('&copy;&nbsp'+current.photographerName.toUpperCase())
    $('.project-image-desc').text(current.desc)
    $('.project-image-desc-original').text(current.originaldesc)
    @attachclick()
    if @container==null
      currentview.getExifInfo(@bindExif)
    else
      return

  bindExif:(photoid,data)=>
    if data && data.camera
      if @exifShown=='undefined'
        @exifShown=false
        $('.chromatic-zoom-exif-not-selected').show()
      $('.camera-info').text(@setExifData(data.camera))
      $('.camera-info').prop('title',data.camera)
      $('.lense-info').text(@setExifData(data.lensModel))
      $('.lense-info').prop('title',data.lensModel)
      $('.shutter-speed').text(@setExifData(data.exposureTime))
      $('.shutter-speed').prop('title',data.exposureTime)
      $('.flash').text(@setExifData(data.flash))
      $('.flash').prop('title',data.flash)
      $('.focalpoint').text('/'+@setExifData(data.fNumber))
      $('.focalpoint').prop('title','/'+data.fNumber)
      $('.focallength').text(@setExifData(data.focalLength))
      $('.focallength').prop('title',data.focalLength)
      $('.iso').text(@setExifData(data.iso))
      $('.iso').prop('title',data.iso)
      $('.software').text(@setExifData(data.software))
      $('.software').prop('title',data.software)
      if @exifShown==true
        $('.chromatic-zoom-exif-selected').show()
        @exifContainer.show()
      else
        $('.chromatic-zoom-exif-not-selected').show()
        @exifContainer.hide()
    else
      @exifContainer.hide()
      $('.chromatic-zoom-exif-not-selected').hide()
      $('.chromatic-zoom-exif-selected').hide()

  setExifData: (data)=>
    if data==null
      return '-'
    if typeof data!='undefind' && data.toString().length==0
      return '-'
    else if typeof data!='undefined' && data.toString().length<15
      return data
    else if typeof data!='undefined'
      return data.toString().substring(0,12)+'...'
    else
      return '-'

  setheight:=>
    @photodetailContainer.css
      height:if @exifComponent.height()<(@infoContainer.height()+28) then (@infoContainer.height()+80) else @exifComponent.height()

  layout: (offset=0, animated) =>
    @current_zoom_photo_view.layout('current', offset, animated,@container) if @current_zoom_photo_view?
    @previous_zoom_photo_view.layout('previous', offset, animated,@container) if @previous_zoom_photo_view?
    @next_zoom_photo_view.layout('next', offset, animated,@container) if @next_zoom_photo_view?
    @setheight()

  attachclick: =>
    $('.project-image-desc').click(->
      $('.project-image-desc').css('display','none')
      $('.project-image-desc-original').css('display','block')
      true
    )

    $('.project-image-desc-original').click(->
      $('.project-image-desc').css('display','block')
      $('.project-image-desc-original').css('display','none')
      true
    )

  rightclick: =>
    return false

  showUrl:=>
    alert(@currentSelectedView.photo.big)   

 