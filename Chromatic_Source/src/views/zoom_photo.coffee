@Chromatic = @Chromatic or {}

_is_css_blur_supported = (->
  _supported = 'dontknow'
  return ->
    return _supported unless _supported == 'dontknow'
    el = $('<div/>')
    $(document.body).append(el)
    el[0].style.webkitFilter = "grayscale(1)"
    _supported = window.getComputedStyle(el[0]).webkitFilter == "grayscale(1)"
    el.remove()
    return _supported
)()

class Chromatic.ZoomPhotoView
  constructor: (parent, photo, options) ->
    @photo = photo
    @el    = $('<div/>')
    @render()
    parent.el.append(@el)
    @exif={}
    #@getExifInfo(photo)

  remove: (photo) =>
    @el.remove()

  render: =>
    @photo_el      = $('<div class="chromatic-zoom-photo"></div>')
    @grain_el      = $('<div class="chromatic-zoom-grain"></div>')
    @background_el = $('<div class="chromatic-zoom-background"></div>')
    
    
    if @photo.big
      big_img        = new Image()
      big_img.onload = => @photo_el.css('backgroundImage', "url(#{@photo.big})")
      big_img.src    = @photo.big
    @photo_el.css('backgroundImage', "url(#{@photo.small})")
    @backgroundenable=@photo.enable
    if @photo.blur && @backgroundenable
      @background_el.css('backgroundImage', "url(#{@photo.blur})")
    else if _is_css_blur_supported() && @backgroundenable
      @background_el.addClass('chromatic-zoom-background-blur').css('backgroundImage', "url(#{@photo.small})")
    else
      @background_el.addClass('chromatic-zoom-background-blur').css('backgroundImage','none')

    @el.append(@photoInfo, @photo_el, @grain_el, @background_el)
    return this

  layout: (pos, offset=0, animated,view) =>
    container = $(window)
    if (@photo.bigHeight/container.height())<0.8 && (@photo.bigWidth/container.width())<0.8
      width=@photo.bigWidth;
      height=@photo.bigHeight;
    else if view != null
      #code for showcaseview
      if @photo.aspect_ratio>1
        width=container.width()
        height=container.width()/@photo.aspect_ratio
      else if container.width() / container.height() > @photo.aspect_ratio
        height = container.height()
        width  = container.height() * @photo.aspect_ratio
      else
        height = container.width() / @photo.aspect_ratio
        width  = container.width()
        #@log=false
    else
      #code for gridview full screen
      if (@photo.bigHeight/container.height()>@photo.bigWidth/container.width())
        height=container.height()
        width=container.height()*@photo.aspect_ratio
      else
        width=container.width()
        height=container.width()/@photo.aspect_ratio
      
    @photo_el.css
      height: height
      width:  width
      top:    (container.height() - height) / 2

    left = switch pos
      when 'previous' then -width-20+offset
      when 'current'
        #if @log
          #0
        #else
          @temp=(container.width()-width)/2+offset
          @photo_el.css('left', @temp)
          @temp
      when 'next'     then container.width()+20+offset

    opacity = switch pos
      when 'current'  then 1-Math.abs(offset)/container.width()*2
      when 'previous' then 0+offset/container.width()*2
      when 'next'     then 0-offset/container.width()*2

    if animated
      @photo_el.stop().animate({opacity: opacity}, 1000, -> $(@).css('left',left))
      @grain_el.stop().animate({opacity: opacity}, 1000)
      @background_el.stop().animate({opacity: opacity}, 1000)
      #@photoInfo.stop().animate({opacity: opacity}, 1000)
      #@photo_el.stop().animate({left: left}, 1000)    
    else
      @photo_el.css('opacity', opacity)
      @grain_el.css('opacity', opacity)
      @background_el.css('opacity', opacity)
      

  getExifInfo:(callback)=>
    @url='http://siftr.co/api/'+@photo.photographerid+'/photos/'+@photo.id+'/exif/'
    $.ajax @url,
      type:'GET',
      success:(data,textStatus,jqXHR)=>
        @exif=data
        callback(@photo.id,data)
      error:(jqXHR,ajaxOptions,errorThrown) =>
        if jqXHR.status==404
          @exif=null
          callback(@photo.id,null)