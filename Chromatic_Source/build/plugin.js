(function() {
  var _linear_partition, _scrollbar_width,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  this.Chromatic = this.Chromatic || {};

  _linear_partition = (function() {
    var _cache;
    _cache = {};
    return function(seq, k) {
      var ans, i, j, key, l, m, n, q, r, ref, ref1, ref2, ref3, s, solution, table, x, y;
      key = seq.join() + k;
      if (_cache[key]) {
        return _cache[key];
      }
      n = seq.length;
      if (k <= 0) {
        return [];
      }
      if (k > n) {
        return seq.map(function(x) {
          return [x];
        });
      }
      table = (function() {
        var l, ref, results;
        results = [];
        for (y = l = 0, ref = n; 0 <= ref ? l < ref : l > ref; y = 0 <= ref ? ++l : --l) {
          results.push((function() {
            var q, ref1, results1;
            results1 = [];
            for (x = q = 0, ref1 = k; 0 <= ref1 ? q < ref1 : q > ref1; x = 0 <= ref1 ? ++q : --q) {
              results1.push(0);
            }
            return results1;
          })());
        }
        return results;
      })();
      solution = (function() {
        var l, ref, results;
        results = [];
        for (y = l = 0, ref = n - 1; 0 <= ref ? l < ref : l > ref; y = 0 <= ref ? ++l : --l) {
          results.push((function() {
            var q, ref1, results1;
            results1 = [];
            for (x = q = 0, ref1 = k - 1; 0 <= ref1 ? q < ref1 : q > ref1; x = 0 <= ref1 ? ++q : --q) {
              results1.push(0);
            }
            return results1;
          })());
        }
        return results;
      })();
      for (i = l = 0, ref = n; 0 <= ref ? l < ref : l > ref; i = 0 <= ref ? ++l : --l) {
        table[i][0] = seq[i] + (i ? table[i - 1][0] : 0);
      }
      for (j = q = 0, ref1 = k; 0 <= ref1 ? q < ref1 : q > ref1; j = 0 <= ref1 ? ++q : --q) {
        table[0][j] = seq[0];
      }
      for (i = r = 1, ref2 = n; 1 <= ref2 ? r < ref2 : r > ref2; i = 1 <= ref2 ? ++r : --r) {
        for (j = s = 1, ref3 = k; 1 <= ref3 ? s < ref3 : s > ref3; j = 1 <= ref3 ? ++s : --s) {
          m = _.min((function() {
            var ref4, results, t;
            results = [];
            for (x = t = 0, ref4 = i; 0 <= ref4 ? t < ref4 : t > ref4; x = 0 <= ref4 ? ++t : --t) {
              results.push([_.max([table[x][j - 1], table[i][0] - table[x][0]]), x]);
            }
            return results;
          })(), function(o) {
            return o[0];
          });
          table[i][j] = m[0];
          solution[i - 1][j - 1] = m[1];
        }
      }
      n = n - 1;
      k = k - 2;
      ans = [];
      while (k >= 0) {
        ans = [
          (function() {
            var ref4, ref5, results, t;
            results = [];
            for (i = t = ref4 = solution[n - 1][k] + 1, ref5 = n + 1; ref4 <= ref5 ? t < ref5 : t > ref5; i = ref4 <= ref5 ? ++t : --t) {
              results.push(seq[i]);
            }
            return results;
          })()
        ].concat(ans);
        n = solution[n - 1][k];
        k = k - 1;
      }
      return _cache[key] = [
        (function() {
          var ref4, results, t;
          results = [];
          for (i = t = 0, ref4 = n + 1; 0 <= ref4 ? t < ref4 : t > ref4; i = 0 <= ref4 ? ++t : --t) {
            results.push(seq[i]);
          }
          return results;
        })()
      ].concat(ans);
    };
  })();

  _scrollbar_width = (function() {
    var _cache;
    _cache = null;
    return function() {
      var div, w1, w2;
      if (_cache) {
        return _cache;
      }
      div = $("<div style=\"width:50px;height:50px;overflow:hidden;position:absolute;top:-200px;left:-200px;\"><div style=\"height:100px;\"></div></div>");
      $(document.body).append(div);
      w1 = $("div", div).innerWidth();
      div.css("overflow-y", "auto");
      w2 = $("div", div).innerWidth();
      $(div).remove();
      return _cache = w1 - w2;
    };
  })();

  Chromatic.GalleryView = (function() {
    function GalleryView(el, photos, options) {
      this.zoom = bind(this.zoom, this);
      this.layout = bind(this.layout, this);
      this.senddelete = bind(this.senddelete, this);
      this.successcallback = bind(this.successcallback, this);
      this.jsonp_custom = bind(this.jsonp_custom, this);
      this.checkdeletephoto = bind(this.checkdeletephoto, this);
      this.checkloading = bind(this.checkloading, this);
      this.checkloadinglow = bind(this.checkloadinglow, this);
      this.checkoverflow = bind(this.checkoverflow, this);
      this.lazyLoad = bind(this.lazyLoad, this);
      this.loadhighresphoto = bind(this.loadhighresphoto, this);
      this.el = $(el).addClass('chromatic-gallery');
      this.photos = _.map(photos.photolist, function(p) {
        if (_.isObject(p)) {
          return p;
        } else {
          return {
            small: p
          };
        }
      });
      this.option = {
        theme: photos.theme,
        isCard: photos.isCard,
        about: photos.about,
        contact: photos.contact
      };
      window.flickrurlcollection = {};
      this.zoom_view = new Chromatic.ZoomView(null, this.photos, this.option);
      this.photo_views = _.map(this.photos, (function(_this) {
        return function(photo) {
          return new Chromatic.GalleryPhotoView(_this, photo, _this.option);
        };
      })(this));
      this.ideal_height = parseInt(this.el.children().first().css('height'));
      $(window).on('resize', _.debounce(this.layout, 100));
      this.layout();
    }

    GalleryView.prototype.loadhighresphoto = function() {
      var self;
      self = this;
      return _.each(this.photo_views, function(p) {
        return p.loadhighres(self.checkloading);
      });
    };

    GalleryView.prototype.lazyLoad = function() {
      var threshold, viewport_bottom, viewport_top;
      this.count = this.photo_views.length;
      threshold = 1000;
      viewport_top = this.el.scrollTop() - threshold;
      viewport_bottom = (this.el.height() || $(window).height()) + this.el.scrollTop() + threshold;
      return _.each(this.photo_views, (function(_this) {
        return function(photo_view) {
          return photo_view.load(_this.checkloadinglow);
        };
      })(this));
    };

    GalleryView.prototype.checkoverflow = function() {
      var error;
      try {
        return $('#left').css({
          maxWidth: $('.navigation')[0].getBoundingClientRect().width
        });
      } catch (_error) {
        error = _error;
      }
    };

    GalleryView.prototype.checkloadinglow = function() {
      this.count = this.count - 1;
      if (this.count === 0) {
        return this.loadhighresphoto();
      }
    };

    GalleryView.prototype.checkloading = function(loaded, photographerid, photoid, alias, source, photourl) {
      if (loaded === '1') {
        return this.checkdeletephoto(photographerid, photoid, alias, source, photourl);
      } else if (loaded === '2') {
        return this.senddelete(alias, photographerid, photoid);
      }
    };

    GalleryView.prototype.checkdeletephoto = function(photographerid, photoid, alias, source, photourl) {
      var jsonp;
      if (source === 'Flickr') {
        jsonp = this.jsonp_custom(this.successcallback, photourl, alias, photographerid, photoid);
        this.url = 'http://www.flickr.com/services/oembed/?format=json&jsoncallback=' + jsonp + '&url=' + encodeURIComponent(photourl);
        return $.ajax(this.url, {
          type: 'GET',
          dataType: 'JSONP',
          jsonpCallback: 'callback',
          error: (function(_this) {
            return function(jqXHR, textStatus, errorThrown) {
              if (jqXHR.status === 404 || jqXHR.status === 401) {
                return _this.senddelete(alias, photographerid, photoid);
              }
            };
          })(this)
        });
      }
    };

    GalleryView.prototype.jsonp_custom = function(callback, photourl, alias, photographerid, photoid) {
      var callback_name;
      callback_name = 'jsonp_callback_' + Math.floor(Math.random() * 100000);
      window[callback_name] = (function(_this) {
        return function(data) {
          callback(data, photourl, alias, photographerid, photoid);
          return delete window[callback_name];
        };
      })(this);
      return callback_name;
    };

    GalleryView.prototype.successcallback = function(data, photourl, alias, photographerid, photoid) {
      if ((photourl.length > 0 && data && !data.url) || (photourl.substring(0, photourl.length - 5) !== data.url.substring(0, data.url.length - 5))) {
        this.senddelete(alias, photographerid, photoid);
      }
    };

    GalleryView.prototype.senddelete = function(alias, photographerid, photoid) {
      var deleteurl;
      deleteurl = '/api/' + photographerid + '/photos/' + photoid + '/';
      return $.ajax(deleteurl, {
        type: 'DELETE'
      });
    };

    GalleryView.prototype.layout = function() {
      var column, ideal_height, ideal_width, index, left, maxHeight, partition, rows, summed_width, viewport_width, weights, widthapprox;
      $(document.body).css('overflowY', 'scroll');
      viewport_width = this.el[0].getBoundingClientRect().width - parseInt(this.el.css('paddingLeft')) - parseInt(this.el.css('paddingRight'));
      if (this.el[0].offsetWidth > this.el[0].scrollWidth) {
        viewport_width = viewport_width - _scrollbar_width();
      }
      $(document.body).css('overflowY', 'auto');
      if (this.option.isCard) {
        ideal_width = 300;
        column = Math.round(viewport_width / ideal_width);
        widthapprox = Math.floor(viewport_width / column);
        if (column < 1) {
          _.each(this.photos, (function(_this) {
            return function(photo, i) {
              return _this.photo_views[i].resize(parseInt(ideal_height * photo.aspect_ratio), ideal_height);
            };
          })(this));
        } else {
          weights = _.map(this.photos, function(p) {
            return parseInt(100 / p.aspect_ratio);
          });
          partition = _linear_partition(weights, column);
          index = 0;
          left = 0;
          maxHeight = 0;
          _.each(partition, (function(_this) {
            return function(column) {
              var row_buffer, top;
              row_buffer = [];
              top = 0;
              _.each(column, function(p, i) {
                return row_buffer.push(_this.photos[index + i]);
              });
              _.each(row_buffer, function(p, i) {
                var height, width;
                width = widthapprox;
                height = width / _this.photo_views[index + i].photo.aspect_ratio;
                _this.photo_views[index + i].resize(width, height);
                _this.photo_views[index + i].setposition(left, top);
                return top = top + height;
              });
              if (top >= maxHeight) {
                maxHeight = top;
              }
              index += column.length;
              return left = left + widthapprox;
            };
          })(this));
          this.el.css({
            height: maxHeight
          });
        }
      } else {
        ideal_height = this.ideal_height || parseInt((this.el.height() || $(window).width()) / 2);
        summed_width = _.reduce(this.photos, (function(sum, p) {
          return sum += p.aspect_ratio * ideal_height;
        }), 0);
        rows = Math.round(summed_width / viewport_width);
        if (rows < 1) {
          _.each(this.photos, (function(_this) {
            return function(photo, i) {
              return _this.photo_views[i].resize(parseInt(ideal_height * photo.aspect_ratio), ideal_height);
            };
          })(this));
        } else {
          weights = _.map(this.photos, function(p) {
            return parseInt(p.aspect_ratio * 100);
          });
          partition = _linear_partition(weights, rows);
          index = 0;
          _.each(partition, (function(_this) {
            return function(row) {
              var row_buffer, summed_ars;
              row_buffer = [];
              _.each(row, function(p, i) {
                return row_buffer.push(_this.photos[index + i]);
              });
              summed_ars = _.reduce(row_buffer, (function(sum, p) {
                return sum += p.aspect_ratio;
              }), 0);
              summed_width = 0;
              _.each(row_buffer, function(p, i) {
                var height, width;
                width = i === row_buffer.length - 1 ? viewport_width - summed_width : parseInt(viewport_width / summed_ars * p.aspect_ratio);
                height = parseInt(viewport_width / summed_ars);
                _this.photo_views[index + i].resize(width, height);
                return summed_width += width;
              });
              return index += row.length;
            };
          })(this));
        }
      }
      this.lazyLoad();
      return this.checkoverflow();
    };

    GalleryView.prototype.zoom = function(photo) {
      return this.zoom_view.show(photo);
    };

    return GalleryView;

  })();

}).call(this);

(function() {
  var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  this.Chromatic = this.Chromatic || {};

  Chromatic.GalleryPhotoView = (function() {
    function GalleryPhotoView(parent, photo, options) {
      this.rightclick = bind(this.rightclick, this);
      this.zoom = bind(this.zoom, this);
      this.unload = bind(this.unload, this);
      this.loadhighres = bind(this.loadhighres, this);
      this.load = bind(this.load, this);
      this.parent = parent;
      this.photo = photo;
      this.theme = options.theme;
      this.isCard = options.isCard;
      this.title = photo.title;
      this.el = $('<div class="chromatic-gallery-photo"></div>');
      if (this.isCard) {
        this.el.addClass('chromatic-card');
      }
      this.elimage = $('<img id="' + this.photo.id + 'image" class="chromatic-gallery-photo-image"></img>');
      this.elcanvas = $('<canvas class="chromatic-gallery-photo-image" id="' + this.photo.id + '"></canvas>');
      this.el.on('contextmenu', this.rightclick);
      parent.el.append(this.el);
      this.el.on('click', this.zoom);
    }

    GalleryPhotoView.prototype.load = function(callback) {
      var image;
      if (this.loaded) {
        return callback();
      }
      this.colorList = ['#f2f4ff', '#f2fffd', '#f3f9ff', '#f2fcff', '#f2fff8', '#f0f6fc', '#f6fff2'];
      this.colorListTheme = ['#382d2d', '#252f31', '#343833', '#313236', '#373237'];
      if (this.theme) {
        this.index = this.photo.key % 5;
        this.color = this.colorListTheme[this.index];
      } else {
        this.index = this.photo.key % 7;
        this.color = this.colorList[this.index];
      }
      this.el.css('background-color', this.color);
      image = new Image();
      image.crossOrigin = 'Anonymous';
      image.onload = (function(_this) {
        return function() {
          _this.photo.aspect_ratio = image.width / image.height;
          _this.photo.width = image.width;
          _this.photo.height = image.height;
          callback();
          _this.elimage.attr('src', _this.photo.low);
          _this.el.append(_this.elimage);
          _this.elimage.addClass('loaded-canvas');
          _this.el.append(_this.elcanvas);
          stackBlurImage(image, _this.photo.id, 30, false);
          return _this.loaded = true;
        };
      })(this);
      image.onerror = (function(_this) {
        return function() {
          return callback();
        };
      })(this);
      if (typeof this.photo.low !== 'undefined' && this.photo.source === 'px') {
        return image.src = 'https://farm6.staticflickr.com/5060/5444250007_1392c5bf6d_s.jpg';
      } else if (typeof this.photo.low !== 'undefined') {
        return image.src = this.photo.low;
      } else {
        return image.src = this.photo.small;
      }
    };

    GalleryPhotoView.prototype.loadhighres = function(callback) {
      var image;
      if (this.loaded && !this.photo.small) {
        return callback('0', this.photo.photographerid, this.photo.id, this.photo.alias, this.photo.source, this.photo.low);
      }
      image = new Image();
      image.onload = (function(_this) {
        return function() {
          _this.photo.aspect_ratio = image.width / image.height;
          _this.photo.width = image.width;
          _this.photo.height = image.height;
          callback('1', _this.photo.photographerid, _this.photo.id, _this.photo.alias, _this.photo.source, _this.photo.low);
          _this.elimage.attr('src', _this.photo.small);
          _this.elcanvas.addClass('loaded-canvas');
          _this.elimage.addClass('loaded-image');
          return _this.loaded = true;
        };
      })(this);
      image.onerror = (function(_this) {
        return function() {
          if (callback) {
            return callback('2', _this.photo.photographerid, _this.photo.id, _this.photo.alias, _this.photo.source, _this.photo.low);
          }
        };
      })(this);
      return image.src = this.photo.small;
    };

    GalleryPhotoView.prototype.unload = function() {
      this.el.css('backgroundImage', "");
      this.el.remove();
      return this.loaded = false;
    };

    GalleryPhotoView.prototype.zoom = function() {
      this.parent.zoom(this.photo);
      return this.parent.el.click();
    };

    GalleryPhotoView.prototype.resize = function(width, height) {
      this.el.css({
        width: width - parseInt(this.el.css('marginLeft')) - parseInt(this.el.css('marginRight')),
        height: height - parseInt(this.el.css('marginTop')) - parseInt(this.el.css('marginBottom'))
      });
      this.top = this.el.position().top;
      return this.bottom = this.top + this.el.height();
    };

    GalleryPhotoView.prototype.setposition = function(left, top) {
      return this.el.css({
        top: top,
        left: left
      });
    };

    GalleryPhotoView.prototype.rightclick = function() {
      return false;
    };

    return GalleryPhotoView;

  })();

}).call(this);

(function() {
  var _scrollbar_width,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  this.Chromatic = this.Chromatic || {};

  $.extend($.easing, {
    easeOutCirc: function(x, t, b, c, d) {
      return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
    }
  });

  _scrollbar_width = (function() {
    var _cache;
    _cache = null;
    return function() {
      var div, w1, w2;
      if (_cache) {
        return _cache;
      }
      div = $("<div style=\"width:50px;height:50px;overflow:hidden;position:absolute;top:-200px;left:-200px;\"><div style=\"height:100px;\"></div></div>");
      $(document.body).append(div);
      w1 = $("div", div).innerWidth();
      div.css("overflow-y", "auto");
      w2 = $("div", div).innerWidth();
      $(div).remove();
      return _cache = w1 - w2;
    };
  })();

  Chromatic.ZoomView = (function() {
    function ZoomView(el, photos, options) {
      this.showUrl = bind(this.showUrl, this);
      this.rightclick = bind(this.rightclick, this);
      this.attachclick = bind(this.attachclick, this);
      this.layout = bind(this.layout, this);
      this.setheight = bind(this.setheight, this);
      this.setExifData = bind(this.setExifData, this);
      this.bindExif = bind(this.bindExif, this);
      this.bindData = bind(this.bindData, this);
      this.showPrevious = bind(this.showPrevious, this);
      this.showNext = bind(this.showNext, this);
      this.show = bind(this.show, this);
      this.close = bind(this.close, this);
      this.portfolio = bind(this.portfolio, this);
      this.mousemovement = bind(this.mousemovement, this);
      this.detachTimer = bind(this.detachTimer, this);
      this.attachTimer = bind(this.attachTimer, this);
      this.attachhideInformationEvent = bind(this.attachhideInformationEvent, this);
      this.hidefirstTime = bind(this.hidefirstTime, this);
      this.hideextrainfo = bind(this.hideextrainfo, this);
      this.startTimer = bind(this.startTimer, this);
      this.noExif = bind(this.noExif, this);
      this.hideExif = bind(this.hideExif, this);
      this.showExif = bind(this.showExif, this);
      this.initExif = bind(this.initExif, this);
      this.container = el;
      this.el = $('<div class="chromatic-zoom"/>');
      this.leftarrow = $('<div class=\"chromatic-zoom-arrow-left\"></div>');
      this.rightarrow = $('<div class=\"chromatic-zoom-arrow-right\"></div>');
      this.grid = $('<div class=\"chromatic-zoom-photo-grid\"></div>');
      this.exifselected = $('<div class=\"chromatic-zoom-exif-selected\"></div>');
      this.exifnotselected = $('<div class=\"chromatic-zoom-exif-not-selected\"></div>');
      this.leftPanel = $('<div class=\"chromatic-left-panel chromatic-panel\"></div>');
      this.rightPanel = $('<div class=\"chromatic-right-panel chromatic-panel\"></div>');
      this.photoInfo = $('<div class="chromatic-zoom-photo-information"></div>');
      this.exifInfo = $('<div class=\"chromatic-zoom-exif-information\"></div>');
      this.navigation = $('<div class=\"chromatic-navigation\"></div>');
      this.clearfloat = '<div style="clear:both"></div>';
      this.navigation.append(this.leftarrow, this.grid, this.rightarrow, this.exifnotselected, this.exifselected, this.clearfloat);
      this.infoContainer = $('<div class=\"project-information\"></div>');
      this.photographerImageContainer = $('<img src=' + photos[0].photographerUrl + '></img>');
      this.pictitle = $('<span class=\"project-image-title\"></span>');
      this.copyright = $('<span class=\"project-image-copyright\"><i style=\"padding-right:3px;\" class=\"glyphicon glyphicon-copyright-mark\"></i></span>');
      this.link = $('<div class=\"links\"><a href="#/about">' + options.about + '</a> | <a href="#/contact">' + options.contact + '</a></div>');
      this.picdesc = $('<span class=\"project-image-desc\"></span>');
      this.picdescoriginal = $('<span class=\"project-image-desc-original\"></span>');
      this.infoContainer.append(this.photographerImageContainer, this.pictitle, this.picdesc, this.picdescoriginal, this.copyright, this.link);
      this.photodetailContainer = $('<div class=\"chromatic-photo-detail\"></div>');
      this.photoInfo = $('<div class=\"chromatic-zoom-photo-information\"></div>');
      this.photoInfo.append(this.infoContainer, this.navigation);
      this.exifContainer = $('<div class=\"project-exif-information\"></div>');
      this.exifComponent = $('<div class="exif-component"></div>');
      this.exifCamera = $('<div class="exif-individual-component">' + '<div class="exif-component-image">' + '<img src="../content/images/exif/camera.png">' + '</div>' + '<div class="info">' + '<span>CAMERA</span>' + '<p class="camera-info"></p>' + '</div>' + '</div>');
      this.exifLense = $('<div class="exif-individual-component">' + '<div class="exif-component-image">' + '<img src="../content/images/exif/lens.png">' + '</div>' + '<div class="info">' + '<span>LENS</span>' + '<p class="lense-info"></p>' + '</div>' + '</div>');
      this.exifShutterSpeed = $('<div class="exif-individual-component">' + '<div class="exif-component-image">' + '<img src="../content/images/exif/shutterspeed.png">' + '</div>' + '<div class="info">' + '<span>SHUTTER SPEED</span>' + '<p class="shutter-speed"></p>' + '</div>' + '</div>');
      this.exifFlash = $('<div class="exif-individual-component">' + '<div class="exif-component-image">' + '<img src="../content/images/exif/flash.png">' + '</div>' + '<div class="info">' + '<span>FLASH</span>' + '<p class="flash"></p>' + '</div>' + '</div>');
      this.exifFPoint = $('<div class="exif-individual-component">' + '<div class="exif-component-image">' + '<img src="../content/images/exif/f-stop.png">' + '</div>' + '<div class="info">' + '<span>F-POINT</span>' + '<p class="focalpoint"></p>' + '</div>' + '</div>');
      this.exifFocalLength = $('<div class="exif-individual-component">' + '<div class="exif-component-image">' + '<img src="../content/images/exif/focal-length.png">' + '</div>' + '<div class="info">' + '<span>FOCAL LENGTH</span>' + '<p class="focallength"></p>' + '</div>' + '</div>');
      this.exifISO = $('<div class="exif-individual-component">' + '<div class="exif-component-image">' + '<img src="../content/images/exif/i-s-o.png">' + '</div>' + '<div class="info">' + '<span>ISO</span>' + '<p class="iso"></p>' + '</div>' + '</div>');
      this.exifSoftware = $('<div class="exif-individual-component">' + '<div class="exif-component-image">' + '<img src="../content/images/exif/software.png">' + '</div>' + '<div class="info">' + '<span>SOFTWARE</span>' + '<p class="software"></p>' + '</div>' + '</div>');
      this.exifComponent.append(this.exifCamera, this.exifShutterSpeed, this.exifFPoint, this.exifISO, this.exifLense, this.exifFlash, this.exifFocalLength, this.exifSoftware, this.clearfloat);
      this.exifContainer.append(this.exifComponent, this.clearfloat);
      this.exifInfo.append(this.exifContainer);
      this.el.on('contextmenu', this.rightclick);
      this.photos = photos;
      this.interval = null;
      this.mousemoveTimeout = null;
      this.el.hide().on('click', '.closeZoom', this.close).on('click', '.chromatic-zoom-arrow-left', this.showPrevious).on('click', '.chromatic-zoom-arrow-right', this.showNext).on('click', '.chromatic-left-panel', this.showPrevious).on('click', '.chromatic-right-panel', this.showNext);
      this.el.on('mousemove', this.mousemovement);
      this.el.append(this.leftPanel, this.rightPanel);
      this._debouncedLayout = _.debounce(((function(_this) {
        return function() {
          return _this.layout();
        };
      })(this)), 100);
      this.currentSelectedView = null;
      if (el !== null) {
        this.el.on('click', '.chromatic-zoom-photo-grid', this.portfolio);
        $(el).append(this.el);
        this.exifnotselected.hide();
        this.exifselected.hide();
        this.photodetailContainer.append(this.photoInfo);
        this.el.append(this.photodetailContainer);
        this.show(photos[0]);
      } else {
        this.el.on('click', '.chromatic-zoom-photo-grid', this.close);
        $('.content-container').append(this.el);
        this.photodetailContainer.append(this.photoInfo, this.exifInfo);
        this.el.append(this.photodetailContainer);
        this.initExif();
      }
      $('.links a').on('click', this.close);
      this.attachhideInformationEvent();
    }

    ZoomView.prototype.initExif = function() {
      $('.chromatic-zoom-exif-not-selected').hide();
      $('.chromatic-zoom-exif-selected').hide();
      this.exifContainer.hide();
      this.exifShown = 'undefined';
      $('.chromatic-zoom-exif-not-selected').on('click', this.showExif);
      return $('.chromatic-zoom-exif-selected').on('click', this.hideExif);
    };

    ZoomView.prototype.showExif = function() {
      $('.chromatic-zoom-exif-not-selected').hide();
      $('.chromatic-zoom-exif-selected').show();
      this.exifShown = true;
      this.setheight();
      return this.exifContainer.show();
    };

    ZoomView.prototype.hideExif = function() {
      $('.chromatic-zoom-exif-not-selected').show();
      $('.chromatic-zoom-exif-selected').hide();
      this.exifShown = false;
      return this.exifContainer.hide();
    };

    ZoomView.prototype.noExif = function() {
      $('.chromatic-zoom-exif-not-selected').hide();
      $('.chromatic-zoom-exif-selected').hide();
      return this.exifContainer.hide();
    };

    ZoomView.prototype.startTimer = function() {
      var self;
      self = this;
      return this.interval = setInterval(function() {
        return self.showNext();
      }, 5000);
    };

    ZoomView.prototype.hideextrainfo = function() {
      return this.photodetailContainer.fadeOut(500);
    };

    ZoomView.prototype.hidefirstTime = function() {
      var self;
      self = this;
      return this.mousemoveTimeout = setTimeout(self.hideextrainfo, 3000);
    };

    ZoomView.prototype.attachhideInformationEvent = function() {
      $('.chromatic-zoom-photo-information').on('mouseenter', this.detachTimer);
      $('.project-exif-information').on('mouseenter', this.detachTimer);
      $('.chromatic-zoom-photo-information').on('mouseleave', this.attachTimer);
      return $('.project-exif-information').on('mouseleave', this.attachTimer);
    };

    ZoomView.prototype.attachTimer = function() {
      this.el.on('mousemove', this.mousemovement);
      if (this.mousemoveTimeout !== null) {
        clearTimeout(this.mousemoveTimeout);
        this.mousemoveTimeout = null;
        return this.hidefirstTime();
      } else if (this.mousemoveTimeout === null) {
        return this.hidefirstTime();
      }
    };

    ZoomView.prototype.detachTimer = function() {
      this.el.off('mousemove');
      this.photodetailContainer.fadeIn(100);
      if (this.mousemoveTimeout !== null) {
        clearTimeout(this.mousemoveTimeout);
        return this.mousemoveTimeout = null;
      }
    };

    ZoomView.prototype.mousemovement = function(event) {
      if (this.lastX !== event.clientX || this.lastY !== event.clientY) {
        this.lastX = event.clientX;
        this.lastY = event.clientY;
        this.photodetailContainer.fadeIn(100);
        if (this.mousemoveTimeout !== null) {
          clearTimeout(this.mousemoveTimeout);
          this.mousemoveTimeout = null;
          return this.hidefirstTime();
        }
      }
    };

    ZoomView.prototype.portfolio = function() {
      this.close();
      if (this.interval) {
        clearInterval(this.interval);
      }
      return window.location.hash = 'portfolio';
    };

    ZoomView.prototype.close = function() {
      if (this.mousemoveTimeout) {
        clearTimeout(this.mousemoveTimeout);
      }
      if (this.interval) {
        clearInterval(this.interval);
      }
      key.unbind('esc');
      key.unbind('enter');
      key.unbind('up');
      key.unbind('left');
      key.unbind('backspace');
      key.unbind('j');
      key.unbind('right');
      key.unbind('k');
      key.unbind('m');
      $(window).off('resize orientationchange', this._debouncedLayout);
      this.el.fadeOut(500, (function(_this) {
        return function() {
          $(document.body).css('paddingRight', '');
          $(document.body).css('overflowY', 'auto');
          _this.previous_zoom_photo_view.remove();
          _this.current_zoom_photo_view.remove();
          _this.next_zoom_photo_view.remove();
          _this.previous_zoom_photo_view = null;
          _this.current_zoom_photo_view = null;
          return _this.next_zoom_photo_view = null;
        };
      })(this));
      return true;
    };

    ZoomView.prototype.show = function(photo) {
      var next, previous;
      $(document.body).css('overflowY', 'hidden');
      if (this.container !== null) {
        this.startTimer();
        key('esc, enter, up', this.portfolio);
      } else {
        $(document.body).css('paddingRight', _scrollbar_width());
        key('esc, enter, up, backspace', this.close);
      }
      key('left, k', _.debounce(this.showPrevious, 100, true));
      key('right, j', _.debounce(this.showNext, 100, true));
      key('m', this.showUrl);
      $(window).on('resize orientationchange', this._debouncedLayout);
      this.el.fadeIn(500);
      this.hidefirstTime();
      if (this.previous_zoom_photo_view) {
        this.previous_zoom_photo_view.remove();
      }
      if (this.current_zoom_photo_view) {
        this.current_zoom_photo_view.remove();
      }
      if (this.next_zoom_photo_view) {
        this.next_zoom_photo_view.remove();
      }
      previous = this.photos[this.photos.indexOf(photo) - 1] || this.photos[this.photos.length - 1];
      this.current = photo;
      next = this.photos[this.photos.indexOf(photo) + 1] || this.photos[0];
      this.previous_zoom_photo_view = new Chromatic.ZoomPhotoView(this, previous);
      this.current_zoom_photo_view = new Chromatic.ZoomPhotoView(this, this.current);
      this.next_zoom_photo_view = new Chromatic.ZoomPhotoView(this, next);
      this.bindData(this.current, this.current_zoom_photo_view);
      this.layout();
      return this.el.show();
    };

    ZoomView.prototype.showNext = function(e) {
      var next;
      if (e) {
        e.preventDefault();
        e.stopPropagation();
        if (this.interval) {
          clearInterval(this.interval);
        }
      }
      if (this.previous_zoom_photo_view != null) {
        this.previous_zoom_photo_view.remove();
      }
      this.previous_zoom_photo_view = this.current_zoom_photo_view;
      this.current_zoom_photo_view = this.next_zoom_photo_view;
      this.current = this.photos[this.photos.indexOf(this.current) + 1] || this.photos[0];
      next = this.photos[this.photos.indexOf(this.current) + 1] || this.photos[0];
      this.next_zoom_photo_view = new Chromatic.ZoomPhotoView(this, next);
      if (this.current_zoom_photo_view != null) {
        this.current_zoom_photo_view.layout('current', 0, true, this.container);
      }
      if (this.previous_zoom_photo_view != null) {
        this.previous_zoom_photo_view.layout('previous', 0, true, this.container);
      }
      this.next_zoom_photo_view.layout('next', 0, false, this.container);
      return this.bindData(this.current, this.current_zoom_photo_view);
    };

    ZoomView.prototype.showPrevious = function(e) {
      var previous;
      if (e) {
        e.preventDefault();
        e.stopPropagation();
        if (this.interval) {
          clearInterval(this.interval);
        }
      }
      this.next_zoom_photo_view.remove();
      this.next_zoom_photo_view = this.current_zoom_photo_view;
      this.current_zoom_photo_view = this.previous_zoom_photo_view;
      this.current = this.photos[this.photos.indexOf(this.current) - 1] || this.photos[this.photos.length - 1];
      previous = this.photos[this.photos.indexOf(this.current) - 1] || this.photos[this.photos.length - 1];
      this.previous_zoom_photo_view = new Chromatic.ZoomPhotoView(this, previous);
      this.next_zoom_photo_view.layout('next', 0, true, this.container);
      this.current_zoom_photo_view.layout('current', 0, true, this.container);
      this.previous_zoom_photo_view.layout('previous', 0, false, this.container);
      return this.bindData(this.current, this.current_zoom_photo_view);
    };

    ZoomView.prototype.bindData = function(current, currentview) {
      this.currentSelectedView = currentview;
      $('.project-image-title').text(current.title.toUpperCase());
      $('.project-image-copyright').html('&copy;&nbsp' + current.photographerName.toUpperCase());
      $('.project-image-desc').text(current.desc);
      $('.project-image-desc-original').text(current.originaldesc);
      this.attachclick();
      if (this.container === null) {
        return currentview.getExifInfo(this.bindExif);
      } else {

      }
    };

    ZoomView.prototype.bindExif = function(photoid, data) {
      if (data && data.camera) {
        if (this.exifShown === 'undefined') {
          this.exifShown = false;
          $('.chromatic-zoom-exif-not-selected').show();
        }
        $('.camera-info').text(this.setExifData(data.camera));
        $('.camera-info').prop('title', data.camera);
        $('.lense-info').text(this.setExifData(data.lensModel));
        $('.lense-info').prop('title', data.lensModel);
        $('.shutter-speed').text(this.setExifData(data.exposureTime));
        $('.shutter-speed').prop('title', data.exposureTime);
        $('.flash').text(this.setExifData(data.flash));
        $('.flash').prop('title', data.flash);
        $('.focalpoint').text('/' + this.setExifData(data.fNumber));
        $('.focalpoint').prop('title', '/' + data.fNumber);
        $('.focallength').text(this.setExifData(data.focalLength));
        $('.focallength').prop('title', data.focalLength);
        $('.iso').text(this.setExifData(data.iso));
        $('.iso').prop('title', data.iso);
        $('.software').text(this.setExifData(data.software));
        $('.software').prop('title', data.software);
        if (this.exifShown === true) {
          $('.chromatic-zoom-exif-selected').show();
          return this.exifContainer.show();
        } else {
          $('.chromatic-zoom-exif-not-selected').show();
          return this.exifContainer.hide();
        }
      } else {
        this.exifContainer.hide();
        $('.chromatic-zoom-exif-not-selected').hide();
        return $('.chromatic-zoom-exif-selected').hide();
      }
    };

    ZoomView.prototype.setExifData = function(data) {
      if (data === null) {
        return '-';
      }
      if (typeof data !== 'undefind' && data.toString().length === 0) {
        return '-';
      } else if (typeof data !== 'undefined' && data.toString().length < 15) {
        return data;
      } else if (typeof data !== 'undefined') {
        return data.toString().substring(0, 12) + '...';
      } else {
        return '-';
      }
    };

    ZoomView.prototype.setheight = function() {
      return this.photodetailContainer.css({
        height: this.exifComponent.height() < (this.infoContainer.height() + 28) ? this.infoContainer.height() + 80 : this.exifComponent.height()
      });
    };

    ZoomView.prototype.layout = function(offset, animated) {
      if (offset == null) {
        offset = 0;
      }
      if (this.current_zoom_photo_view != null) {
        this.current_zoom_photo_view.layout('current', offset, animated, this.container);
      }
      if (this.previous_zoom_photo_view != null) {
        this.previous_zoom_photo_view.layout('previous', offset, animated, this.container);
      }
      if (this.next_zoom_photo_view != null) {
        this.next_zoom_photo_view.layout('next', offset, animated, this.container);
      }
      return this.setheight();
    };

    ZoomView.prototype.attachclick = function() {
      $('.project-image-desc').click(function() {
        $('.project-image-desc').css('display', 'none');
        $('.project-image-desc-original').css('display', 'block');
        return true;
      });
      return $('.project-image-desc-original').click(function() {
        $('.project-image-desc').css('display', 'block');
        $('.project-image-desc-original').css('display', 'none');
        return true;
      });
    };

    ZoomView.prototype.rightclick = function() {
      return false;
    };

    ZoomView.prototype.showUrl = function() {
      return alert(this.currentSelectedView.photo.big);
    };

    return ZoomView;

  })();

}).call(this);

(function() {
  var _is_css_blur_supported,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  this.Chromatic = this.Chromatic || {};

  _is_css_blur_supported = (function() {
    var _supported;
    _supported = 'dontknow';
    return function() {
      var el;
      if (_supported !== 'dontknow') {
        return _supported;
      }
      el = $('<div/>');
      $(document.body).append(el);
      el[0].style.webkitFilter = "grayscale(1)";
      _supported = window.getComputedStyle(el[0]).webkitFilter === "grayscale(1)";
      el.remove();
      return _supported;
    };
  })();

  Chromatic.ZoomPhotoView = (function() {
    function ZoomPhotoView(parent, photo, options) {
      this.getExifInfo = bind(this.getExifInfo, this);
      this.layout = bind(this.layout, this);
      this.render = bind(this.render, this);
      this.remove = bind(this.remove, this);
      this.photo = photo;
      this.el = $('<div/>');
      this.render();
      parent.el.append(this.el);
      this.exif = {};
    }

    ZoomPhotoView.prototype.remove = function(photo) {
      return this.el.remove();
    };

    ZoomPhotoView.prototype.render = function() {
      var big_img;
      this.photo_el = $('<div class="chromatic-zoom-photo"></div>');
      this.grain_el = $('<div class="chromatic-zoom-grain"></div>');
      this.background_el = $('<div class="chromatic-zoom-background"></div>');
      if (this.photo.big) {
        big_img = new Image();
        big_img.onload = (function(_this) {
          return function() {
            return _this.photo_el.css('backgroundImage', "url(" + _this.photo.big + ")");
          };
        })(this);
        big_img.src = this.photo.big;
      }
      this.photo_el.css('backgroundImage', "url(" + this.photo.small + ")");
      this.backgroundenable = this.photo.enable;
      if (this.photo.blur && this.backgroundenable) {
        this.background_el.css('backgroundImage', "url(" + this.photo.blur + ")");
      } else if (_is_css_blur_supported() && this.backgroundenable) {
        this.background_el.addClass('chromatic-zoom-background-blur').css('backgroundImage', "url(" + this.photo.small + ")");
      } else {
        this.background_el.addClass('chromatic-zoom-background-blur').css('backgroundImage', 'none');
      }
      this.el.append(this.photoInfo, this.photo_el, this.grain_el, this.background_el);
      return this;
    };

    ZoomPhotoView.prototype.layout = function(pos, offset, animated, view) {
      var container, height, left, opacity, width;
      if (offset == null) {
        offset = 0;
      }
      container = $(window);
      if ((this.photo.bigHeight / container.height()) < 0.8 && (this.photo.bigWidth / container.width()) < 0.8) {
        width = this.photo.bigWidth;
        height = this.photo.bigHeight;
      } else if (view !== null) {
        if (this.photo.aspect_ratio > 1) {
          width = container.width();
          height = container.width() / this.photo.aspect_ratio;
        } else if (container.width() / container.height() > this.photo.aspect_ratio) {
          height = container.height();
          width = container.height() * this.photo.aspect_ratio;
        } else {
          height = container.width() / this.photo.aspect_ratio;
          width = container.width();
        }
      } else {
        if (this.photo.bigHeight / container.height() > this.photo.bigWidth / container.width()) {
          height = container.height();
          width = container.height() * this.photo.aspect_ratio;
        } else {
          width = container.width();
          height = container.width() / this.photo.aspect_ratio;
        }
      }
      this.photo_el.css({
        height: height,
        width: width,
        top: (container.height() - height) / 2
      });
      left = (function() {
        switch (pos) {
          case 'previous':
            return -width - 20 + offset;
          case 'current':
            this.temp = (container.width() - width) / 2 + offset;
            this.photo_el.css('left', this.temp);
            return this.temp;
          case 'next':
            return container.width() + 20 + offset;
        }
      }).call(this);
      opacity = (function() {
        switch (pos) {
          case 'current':
            return 1 - Math.abs(offset) / container.width() * 2;
          case 'previous':
            return 0 + offset / container.width() * 2;
          case 'next':
            return 0 - offset / container.width() * 2;
        }
      })();
      if (animated) {
        this.photo_el.stop().animate({
          opacity: opacity
        }, 1000, function() {
          return $(this).css('left', left);
        });
        this.grain_el.stop().animate({
          opacity: opacity
        }, 1000);
        return this.background_el.stop().animate({
          opacity: opacity
        }, 1000);
      } else {
        this.photo_el.css('opacity', opacity);
        this.grain_el.css('opacity', opacity);
        return this.background_el.css('opacity', opacity);
      }
    };

    ZoomPhotoView.prototype.getExifInfo = function(callback) {
      this.url = 'http://siftr.co/api/' + this.photo.photographerid + '/photos/' + this.photo.id + '/exif/';
      return $.ajax(this.url, {
        type: 'GET',
        success: (function(_this) {
          return function(data, textStatus, jqXHR) {
            _this.exif = data;
            return callback(_this.photo.id, data);
          };
        })(this),
        error: (function(_this) {
          return function(jqXHR, ajaxOptions, errorThrown) {
            if (jqXHR.status === 404) {
              _this.exif = null;
              return callback(_this.photo.id, null);
            }
          };
        })(this)
      });
    };

    return ZoomPhotoView;

  })();

}).call(this);

(function() {
  $.fn.extend({
    chromatic: function(photos, options) {
      new Chromatic.GalleryView(this, photos, options);
      return this;
    },
    chromaticZoom: function(photos, options) {
      new Chromatic.ZoomView(this, photos, options);
      return this;
    }
  });

}).call(this);
